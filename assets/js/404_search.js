'use strict';

{{ $searchDataFile := printf "%s.search-data.json" .Language.Lang }}
{{ $searchData := resources.Get "search-data.json" | resources.ExecuteAsTemplate $searchDataFile . | resources.Minify | resources.Fingerprint }}
{{ $searchConfig := i18n "bookSearchConfig" | default "{}" }}

document.addEventListener("DOMContentLoaded", function() {
    const searchDataURL = '{{ $searchData.RelPermalink }}';
    const indexConfig = Object.assign({{ $searchConfig }}, {
      doc: {
        id: 'id',
        field: ['title', 'content'],
        store: ['title', 'href', 'section']
      }
    });

    fetch(searchDataURL)
      .then(pages => pages.json())
      .then(pages => {
        const flex = FlexSearch.create('balance', indexConfig);
        flex.add(pages);
        // Get the current path
        var path = window.location.pathname;
        // Remove leading and trailing slashes
        const term = path.replace(/^\/|\/$/g, '');
        // Perform the search
        var results = flex.search(term, 10);
        // Get the search results div
        var resultsDiv = document.getElementById('search-results');
        // Iterate over the results
        for (var i = 0; i < results.length; i++) {
            // Create a new link for each result
            var link = document.createElement("a");
            link.href = results[i].href;
            link.text = results[i].title + " (" + results[i].section + ")";
            // Create a new paragraph for each result
            var para = document.createElement("p");
            para.appendChild(link);
            // Append each result to the search results div
            resultsDiv.appendChild(para);
        }
    });
});
