---
title: KWGT FAQ
type: docs
categories:
  - FAQ
tags:
  - Introduction
  - KWGT
  - FAQ
---
# KWGT Frequently Asked Questions

## Why KWGT needs to display a Notification?

Kustom to work properly needs to be always running, the notification can be disabled by removing Kustom from battery optimized apps, a full explanation is available at [here](https://docs.kustom.rocks/docs/general_information/persistent_notifciation/).

## Can Kustom Widget update every second?

Android Widgets do not know if they are currently displayed or not, this means that they will run also when you are using another app like a browser (even if Kustom widget never updates when screen is off it still doesn't know if you are actually looking at it or not when screen is on), so, from a technical point of view KWGT could update every second but the feature has been disabled to avoid battery issues or performance issues when very complex presets are being used. This could change in the future if Android OS or Launchers will add a better support for this

## Widget resizes itself / size is not right

Please ensure that widget sizing is not set to "Auto" in app settings, when set to auto KWGT will follow launcher instructions to get size and this might make it resize in a weird manner depending on how good launcher is in giving this information. Also ensure that your device rotation is detected correctly in the settings it should show something like "Default (Landscape)" if its not your correct location force the correct one. Finally ALWAYS resize the widget before loading it, so add the widget and then long press on it and drag the borders to bring it to its final size, then finally click on it to load the preset. If not already scaled correctly adjust widget size using "scale" option in the layer tab.

## How Kustom Widget compares to Kustom Wallpaper?

KLWP is a live wallpaper and as such has some advantages like very fast updates, animations and is Launcher indipendent, KWGT on the other hand might be used with other Live Wallpapers, in lock screen apps and on Launchers that do not support LWPs (like Buzz). So it's really up to you, if you want to theme your entire screen probably KLWP is the best option, if you just need a small clock then you should check KWGT out. And, off course, you can use them both together if you like!

## Can i use my own fonts?

Sure! In your SD card you should find a directory called Kustom with a folder named "fonts" inside, put your ttf/otf items there, they will then be listed in the editor.

## Someone sent me a file in ".kwgt" or ".kwgt.zip" format, where do i put it?

Just press "Load Preset", then the "folder" button on top menu, then select "Import" from the options. If you want to do that manually or from PC you need to copy the file as it is (without decompressing) in the SD card under "Kustom/widgets/" directory, the directory should already be there. Once copied the widgets will show up in the "sd card" section when pressing "load preset" from the main settings. Please always ensure that the zip file is not corrupted and that the extension has not been changed (it has to be ".kwgt.zip" or ".kwgt", both are the same)

## How can i make the widget fill the entire screen width?

By default Android introduces some padding for the widgets so this is not possible with the standard Launcher, if you have Nova you need to remove widget padding in the settings and then long press on the widget and select the option "padding" from the popup, this will remove the padding from the Widget.

## Installed PlayStore / APK Packs are not showing up in the "installed" list

This usually happens when some optimization app is trying to block the skin packages to provide files to Kustom, to fix this you need to whitelist the skin packs from any of those apps. For example Purify and Greenify are known to cause this issue and you need to specifically disable what they call "deep hibernation" to solve this. On Samsung the tool called Smart Management has also been reported to cause problems. On Huawei you must activate skin APKs by going into "Phone manager / Automatic Start / Secondary Activation Management" and activate the packages.

## Why you support Android KitKat or better only?

Kustom uses features that are only available from Android 4.4 and cannot be ported to a lower version

## Next alarm always returns random values

If you are using a third party Alarm software then your app might not be supported or it might be caused by Tasker if you checked the option "Use Reliable Alarms" in preferences (in this case you need to disable it to have reliable results)

## KWGT is stuck (also minutes do not update)

Please ensure that nothing prevents Kustom service to run in the background, for example some tool might not allow processes to run as services (like Greenify, Purify or Samsung's Smart Management), please ensure that you properly whitelist Kustom from those

## My preset was using Google Maps and now is showing just blank

Unfortunately some time ago Google has removed the ability to fetch maps data without using a Google Maps API key, there is no solution to this other than registering an API key and then use it inside the maps call. Probably in the future theme owners will add the ability to add an API key or embed their own but so far you would need to do it manually to make it work.

## I cannot use custom fonts anymore on my Huawei phone

On Huawei phones there is a Theming app, please ensure that you did not change the font there, if you did then switch it back to the default one. Unfortunately the way Huawei implemented theming is by overriding core OS functionatlies