---
title: Wanna help adding new built in presets? Thanks! Here is how!
type: docs
categories:
  - More
tags:
  - More
  - built in
  - presets
---

# Wanna help adding new built in presets? Thanks! Here is how!

A lot of users complain that built in presets sucks a bit, yeah, i know, they are right and i am not good creating them so if you want to help i would really be happy to add your presets to the builtins keeping off course name and email of you in so more can be searched in the store if you publish any pack there.

Requirements for built ins:

* Preset or Komponent has to be fully unlocked
* No external fonts, only built in ones can be used
* No external images unless total size of images is LESS than 20KB
* Komponents inside a preset or another Komponent can be used ONLY IF they respect same rules here, so no external fonts, no images, unlocked and so on
* Globals should be used to change basic theming like fonts and colors
* Main layers or objects should have a meaningful name
* You can use Komponents inside presets if Komponents 

If all rules above are fine just send it to [frank.bmonza@gmail.com](mailto:frank.bmonza@gmail.com) you'll get some promo codes too :)

P.S. for convenience here is the list of fonts that can be used:

\- CutiveMono-Regular  
\- DancingScript-Regular  
\- IndieFlower  
\- PoiretOne-Regular  
\- Roboto-Black  
\- Roboto-Bold  
\- Roboto-Light  
\- Roboto-Medium  
\- Roboto-Regular  
\- Roboto-Thin  
\- RobotoCondensed-Bold  
\- RobotoCondensed-Light  
\- RobotoCondensed-Regular  
\- RobotoSlab-Bold  
\- RobotoSlab-Light  
\- RobotoSlab-Regular  
\- RobotoSlab-Thin  
\- Sarpanch-Regular  
\- Ubuntu-Regular  
\- Walkway