---
title: Kustom Adaptive Icon Contest Winners
type: docs
categories:
  - More
tags:
  - More
  - adaptive icon
  - winners
---

# Kustom Adaptive Icon Contest Winners

Oh man this was really a good competition! Kudos to all the designers who participated you really did an awesome job, if you didnt win really looking forward for seeing those icons in an icon pack soon! So, WHAT ABOUT THE FINAL RESULTS?!?!?! Here we go, this is what the community decided and i have nothing to say to my wonderful community other than accepting their choice which i actually DO LOVE, say welcome to the new Kustom Icon

Final results
-----------------

**Number one goes to [Max Patchs](https://twitter.com/maxpatchs)! Congrats!!!!**  
![](https://s3.amazonaws.com/chd-data/files/2759/792937449_c9009614222f4d2b474b11237bb4c75b_3779/01_MaxPatchs2.png)

And then? What about the others??? So, second place goes to **Za Vukodlak**! Very close!

![](https://s3.amazonaws.com/chd-data/files/2759/2544018560_907a6f7152641ba8348df8489e24a447_3780/06_ZaVukodlak.png)

Following in random order (as they placed really right one eachother) are **[LKN9X](https://twitter.com/lkn9x)**

![](https://s3.amazonaws.com/chd-data/files/2759/449409509_33b751cf210ace048ac10efa22bfebfc_3781/03_LKN9X.png)

**[Design Ironic](https://twitter.com/DesignIronic)**

![](https://s3.amazonaws.com/chd-data/files/2759/1724055993_47fdafb633f7e50ccc1a94bfb76d4c5a_3782/04_Design_Ironic.jpg)

And **[Sajid Shaik](https://twitter.com/yosajidshaik)**!

![](https://s3.amazonaws.com/chd-data/files/2759/3746045624_fc4ff64bb9b64159b0b890135d2e6eef_3783/08_SajidShaik.png)

Runners up
-----------------

As i said all designs were really good so all worth a mention, again, random order, we have **[Mowmo](https://twitter.com/_mowmo_)**

![](https://s3.amazonaws.com/chd-data/files/2759/2250969026_98cac99760f95bf98ddbe1527a0a60d3_3784/07_Mowmo.png)

**Icon Dad Labs**

![](https://s3.amazonaws.com/chd-data/files/2759/427498602_5358df1511ebbf149e447cd39244a0ed_3786/05_IconDabLabs.png)

And **Jonah Brawley**!

![](https://s3.amazonaws.com/chd-data/files/2759/3985389076_83e384be25bf37a98f32840ce7b16460_3787/02_JonahBrawley.png)