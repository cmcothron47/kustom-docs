---
title: Unread SMS and missed calls counters
type: docs
categories:
  - More
tags:
  - More
  - unread sms
  - missed calls
  - counters
---

# Unread SMS and missed calls counters

Unfortunately Google has now [restricted access to the SMS and Calls](https://support.google.com/googleplay/android-developer/answer/9047303) permissions, due to this change Kustom is not able to reliably read SMS and missed calls and the unred plugin for devices running on Android 5 or lower cannot be distributed anymore on the Play Store. Capability is still there if you grant Kustom access to notifications but it will just give you the status of what its actively in your notification bar, real unread and missed calls info wont be displayed anymore since the stock app cannot be queried without SMS and CALLS permissions.

**There is a workaround**, if you still want to use this capability you can use the official Kustom Unread plugin by downloading it manually at [this URL](http://kustom.rocks/unread/apk), Kustom from 3.38 will detect the plugin and use that instead of the notification based unread counters. To install the APK from that location you will need to enable the installation from third party location on your browser. Another option is to use a task scheduler app since those apps (es Tasker) are allowed to read SMS and Calls, you will then need to use the Tasker plugin to send data to Kustom.