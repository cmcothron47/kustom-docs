--- 
title: KAPK Walls Format
type: docs
categories:
  - More
tags:
  - More
  - kapk
  - walls format
---

# KAPK Walls Format

Kustom APK Dashboard uses cloud-based wallpapers, meaning you need to host your wallpaper images on the internet. An easy and free way to do this is a public [GitHub](https://github.com) or [BitBucket](https://bitbucket.org) repository. Both sites allows you to host files of any type (provided you own the rights to do so), and they are fast. You can also just store the JSON (see below) in them and then use public image sites to store the walls.

So, first step is to collect all your images URLs and create the JSON like the one below, example is taken from [kustom.rocks/apkmaker/walls](https://kustom.rocks/apkmaker/walls), the "thumbnail" part is optional, you can remove it if you want, please always be aware that the latest entry in a list doesn't have a comma, you can test your JSON once finished using a JSON validator site like [this one](https://jsonlint.com/) for example. The format is the same used by Polar Dashboard so if you are migrating to/from that you can just reuse the same.

    {
      "wallpapers": [
        {
          "author": "Kaique Rocha",
          "url": "https://static.pexels.com/photos/53756/pexels-photo-53756.jpeg",
          "name": "Night City Street",
          "thumbnail": "https://images.pexels.com/photos/53756/pexels-photo-53756.jpeg?h=350&auto=compress&cs=tinysrgb"
        },
        {
          "author": "No One",
          "url": "https://static.pexels.com/photos/268832/pexels-photo-268832.jpeg",
          "name": "Drops",
          "thumbnail": "https://images.pexels.com/photos/268832/pexels-photo-268832.jpeg?h=350&auto=compress&cs=tinysrgb"
        }
      ]
    }

Once you have the file just upload it as a [GitHub Gist](https://gist.github.com/) or a [BitBucket Snippet](https://bitbucket.org/snippets) or anywhere else and provide the URL to the [APK Maker](https://kustom.rocks/apkmaker) app or use it in the right entry on the [APK Skin Sample](https://bitbucket.org/frankmonza/kustomskinsample) repo. 

Enjoy!