---
title: Create a bug report
type: docs
categories:
  - More
tags:
  - More
  - bug
  - report
---

# Create a bug report

### To send an Android Bug Report do this

* Enable Android Developer Options and USB debugging (step by step instructions [here](https://developer.android.com/studio/debug/dev-options#enable))
* Reproduce the issue
* Go into the Developer Options and press Bug Report (step by step instructions [here](https://developer.android.com/studio/debug/bug-report#bugreportdevice))
* Generated file might be big (>10MB), send me that via email at help@kustom.rocks

### Miui (Xiaomi) devices

If you are on MIUI then the procedure to activate bug reports is different please check this article: [https://in.c.mi.com/thread-75776-1-0.html](https://in.c.mi.com/thread-75776-1-0.html)