---
title: How KWGT compares with Zooper / Buzz / UCCW?
type: docs
categories:
  - 
tags:
  - 
  - 
  - 
---

# How KWGT compares with Zooper / Buzz / UCCW?

Here are some of the main advantages of KWGT over its competitors, Zooper, Buzz and UCCW (go check them out!). This list is not exhaustive it's just a starting point, probably there is much much more :)  
  
Anyway, in KWGT:

* You have Komponents which are like widgets inside widgets or reusable items inside the editor. So for example you can create a clock and redistribute it as a module for KWGT, users will just add your clock and change the options you expose (like colors and font), there are [many examples](https://play.google.com/store/search?q=komponent&c=apps) online on the store already or you can check the dedicated thread above 
* You can download content directly from HTTP, so you can have a Google Map with the city you are in directly in the widget background if you want, or use Street View, or you can have a Komponent that does that for you [like this](https://play.google.com/store/apps/details?id=com.jonaseymour.kustomisedmap), you can add it with one click 
* You have global variables, so you can set a color once and then use it whenever you like, so, when you want to change it you just change the global and it will change the color everywhere in one click, this allows users of your theme to change color easily 
* You can create touch actions that will hide / show elements based on touch, so you can show full weather data when someone clicks on a weather icon and then hide it automatically after X seconds 
* You have containers, so you can group items inside an object, move them together, apply effects and rotation to the entire group 
* You have built in music support, so music will work without any additional tool, no music utilities needed, it just works 
* You have more effects, like long shadows, morphable text, blur images and so on 
* You can extract colors from images, cover art or anything downloaded from the internet (like kolorette does, but builtin) 
* You can use formulas on any property you like, no need to use "advanced parameters", you just turn a property into a formula and then you can do anything you like 
* You have full support for different location, so you can apply a location to any group and make a world clock in seconds, locations are then easily accessible from the settings 
* You have much more functions, you can download text from HTTP, use regular expressions, parse RSS, XML and JSON, do math with dates, create timers 
* KWGT supports SVG (vector) files, so you can add images that will perfectly scale on any screen
* Much better resizing, the widget will resize automatically and touch will always work on any module regardless of the orientation 
* You can create APK Skin Packs in seconds and without ANY development knowledge with the [APK Maker APP](http://kustom.rocks/apkmaker)
* Status Bar Notifications support with both text and images
* Google Fitness data with segments, duration, calories, steps, historical stats and more
* Last but not least KWGT is much much more battery efficient, you can check this easily with a good battery monitor like [better battery stats plus](https://play.google.com/store/apps/details?id=com.asksven.betterbatterystats), Kustom has 2 processes so you can clearly see the difference between the editor and the real widget/wallpaper service)