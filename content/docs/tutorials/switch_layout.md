---
title: Switch an image or layout if day or night (or by time)
type: docs
categories:
  - Tutorials
tags:
  - Tutorials
  - day
  - night
  - switch
---

# Switch an image or layout if day or night (or by time)

howing an image based on a condition is pretty simple, to switch between one image and another based on day/night status you can do the following:

* Create an OverlapLayer that will contain both images
* Add an Image object to the layer just created and pick the night image
* Select the "Opacity" property and switch it to "formula" mode using the calculator icon
* Click it and enter following code `$if(ai(isday), 0, 100)$` this will set opacity to 0 during the day and to 100 in the night
* Add another Image object to the layer and pick the day image
* Switch Opacity to fomula as above and enter `$if(ai(isday), 100, 0)$`
* Done!

This approach can be used also with text (using the color property) or with entire layers using a formula in the "visibility" property of the layer tab and returning either ALWAYS or NEVER.

### Advanced example, switch 3 layouts every 30 minutes

Let's try something more complex, say that you want to switch between 3 groups or komponents every 30 minutes, this can be dowe as follows

* Create your 3 groups or komponents
* Select the visibility option of the first group/komponent and turn it into a formula as explained above
* Use `$if((df(S) / (30*60)) % 3 = 0, ALWAYS, NEVER)$`, this means that we first divide time in slices of 30 minutes (so 30*60 in seconds) and then check if the remaining part of the division by 3 is 0
* Use the same formula on the other 2 objects replacing `= 0` with `= 1` and `= 2` 
* Done! You can use this approach with any number of objects just replace the `% 3` with the number you need and the `30*60` with the time slot you want