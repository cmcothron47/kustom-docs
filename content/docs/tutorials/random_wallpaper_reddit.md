---
title: Random wallpaper daily from reddit.com/r/EarthPorn
type: docs
categories:
  - Tutorials
tags:
  - Tutorials
  - random
  - wallpaper
---

# Random wallpaper daily from reddit.com/r/EarthPorn

Lets say you want to get a nice random image from the wonderful sub reddit at [https://reddit.com/r/EarthPorn](https://reddit.com/r/EarthPorn)

Nothing easier:

* Start a blank preset
* Go to the background tab
* Select the bitmap property
* Switch to a formula
* Enter this formula: `$tv(reg, wg("https://www.reddit.com/r/EarthPorn/.rss?refresh="+df(D), url, "i.redd.it", 0), "&.*", "")$`

Save and done!

You can update this more frequently by changing for example df(D) to df(H) and it will be updated at the hour (if content changed)