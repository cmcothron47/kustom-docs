---
title: How to turn DND On/Off using Tasker
type: docs
categories:
  - Tutorials
tags:
  - Tutorials
  - DND
  - Tasker
  - Do Not Disturb
---

# How to turn DND On/Off using Tasker

Though it is currently not possible to manage DND with the Kustom app alone, with the help of Tasker, this can be achieved. This Tutorial can also be modified and used with other tasks that Tasker can perform. 

First, create the necessary tasks in Tasker.

{{< col >}}

### Turn on DND

1. Open Tasker
2. Go to the TASKS tab
3. Add a new task by tapping on the "+" button
4. Enter the task name. Let's call this "DND On"
5. On the Task Edit page, tap on the "+" button to add an action
6. Select Audio >> Do Not Disturb
7. Change the Mode value to "No Interruptions" 

<--->

<img src="https://imagizer.imageshack.com/img923/3602/WaYEF3.gif" alt="Tasker Turn On DND" width="300">

<--->

{{< /col >}}

{{< col >}}

### Turn off DND

1. Open Tasker
2. Go to the TASKS tab
3. Add a new task by tapping on the "+" button
4. Enter the task name. Let's call this "DND Off"
5. On the Task Edit page, tap on the "+" button to add an action
6. Select Audio >> Do Not Disturb
7. Change the Mode value to "Allow All" 

<--->

<img src="https://imagizer.imageshack.com/img924/9282/RJnlqh.gif" alt="Tasker Turn On DND" width="300">

<--->

{{< /col >}}

Once you have the tasks created, you can start creating the KWGT Controls. For the sake of simplicity, we'll just create two shapes. One, to turn on DND, and the other to turn it off.

{{< col >}}

### Add "Turn on DND Control"

1. Create an empty KWGT Preset
2. Tap on the "+" button from the top
3. Select "Shape" (change size, position, name, and color, as you please)
4. Open the shape element
5. Go to the Touch tab
6. Tap on the "+" from the top right corner
7. Tap on the added touch function from the bottom (Single - None) 
8. Tap on the action (Action - None)
9. Select "Launch Shorcut"
10. Tap "Shortcut"
11. Select "Tasker Shortcut" and pick your "DND On" task. **
12. Tap on the back arrow from the top left

<--->

<img src="https://imagizer.imageshack.com/img924/5002/uifYYA.gif" alt="Tasker Turn On DND" width="300">

<--->

{{< /col >}}

{{< col >}}

### Add "Turn on DND Control"

1. Tap on the "+" button from the top to add another element
2. Select "Shape" (change size, position, name, and color, as you please)
3. Open the shape element
4. Go to the Touch tab
5. Tap on the "+" from the top right corner
6. Tap on the added touch function from the bottom (Single - None) 
7. Tap on the action (Action - None)
8. Select "Launch Shorcut"
9. Tap "Shortcut"
10. Select "Tasker Shortcut" and pick your "DND Off" task. **
11. Tap on the back arrow from the top left

<--->

<img src="https://imagizer.imageshack.com/img924/2361/syjx9T.gif" alt="Tasker Turn On DND" width="300">

<--->
{{< /col >}}















