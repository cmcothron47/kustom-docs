---
title: Sharing simple modules via text
type: docs
categories:
  - Tutorials
tags:
  - Tutorials
  - sharing
  - modules
  - text
---

# Sharing simple modules via text

So you have created a wonderful signal indicator using shapes and you want all your friends to use it, right? Ok, let's see how to share it via email / g+ / whatever.

### Share

* Open the advanced editor
* Select the module you want to share
* Press the share icon on the action bar
* Select your preferred app and send! As long as you do not change anything inside the `##KUSTOMCLIP##` borders you can write anything you like.

### Import

* Copy the entire message that has been shared with you (you do not need to copy only the `##KUSTOMCLIP##` part, just copy everything, for example in G+ you can press the "copy text" option in the drop down menu)
* Open Kustom, you will see a Toast saying that "clip has been imported"
* You are done, you can now paste the item wherever you like

### Limits

* Bitmaps, custom fonts or resources in general are not copied
* Globals are currently not copied