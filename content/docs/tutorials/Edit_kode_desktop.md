---
title: How to edit kode from your desktop
type: docs
categories:
  - Tutorials
tags:
  - Tutorials
  - mirroring
  - desktop
---

# How to edit kode from your desktop

The limited screen real estate of mobile devices makes it a bit tricky to design a Kustom widget or theme. To get around this, you can use mirroring software so you can perform this task from your desktop with the full use of your keyboard and mouse.

Here are some recommended free tools from the community to achieve this,

### [Vysor](https://www.vysor.io/)

### [Air Droid](https://www.airdroid.com/)

### [TeamViewer](https://www.teamviewer.com/en/info/quicksupport/)

Instead of simply mirroring your phone, you can also use emulators and run Android itself from your desktop. Some options to do this are:

### [Bluestacks](https://www.bluestacks.com/)

### [Windows Phone link](https://www.microsoft.com/en-us/windows/sync-across-your-devices)

### [Genymotion](https://www.genymotion.com/)

### [Android emulator from Android developer](https://developer.android.com/studio/run/emulator)

There are a lot of other options out there. These are just the most common ones. If you're using one that you think is better, please share it with the community.