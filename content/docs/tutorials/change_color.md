---
title: Change color based on a condition
type: docs
categories:
  - Tutorials
tags:
  - Tutorials
  - color
  - condition
---

# Change color based on a condition

Let's say that you want to change the color of a Shape based on the status of the battery, so, for example, you want a shape to be yellow when the battery is discharging and green when charging, this can be done in many ways, so let's start for the simplest example.

### Simple color change

* Open the Advanced Editor and Create a Shape using the "+" on the top right
* Change the Shape aspect according to your needs
* Go in the "Paint" section and select the color preference
* On the top bar the "formula" icon will appear, select it
* Now the preference has been switched to a "Formula" preference so it's now controlled by an expression rather than a simple value, click the preference to edit the Formula
* Type `$if(bi(charging) = 0, #FFFF00, #00FF00)$` and press the "ok" button on top right
* Done! Your shape will be `#FFFF00` when battery is discharging and `#00FF00` red otherwise

### Color change with globals

What if you want to change the color based on the battery status but also keep it easy to change those colors using the picker? Well, in this case you need to introduce the Globals, so:

* Open the Advanced Editor and open the "Globals" sectionAdd 2 Color Globals with name "Battery" and "Charging"
* Pick the color for both
* Add a Shape as in the example above and set the color preference to "Formula" as above
* Type `$if(bi(charging) = 0, gv(battery), gv(charging))$` and press the "ok" button on top right
* Done! Your shape will have "Battery" global color when not charging or "Charging" color otherwise