---
title: Create an Arc / Slice progress bar using a Shape
type: docs
categories:
  - Tutorials
tags:
  - Tutorials
  - arc
  - progress
  - slice
---

# Create an Arc / Slice progress bar using a Shape

Currently Kustom does not support Arcs or Slices in progress bars, so, in the meanwhile you need to use the following workaround:

* Add an Arc shape or a Circle Slice shape
* Go to FX and set Texture option to Sweep Gradient
* Set width to 1, this will basically transform the shape in a flat progress bar
* Now the "offset" option is your progress, select it, then press the calculator on top to use a formula
* Enter any formula that goes from 0 to 100, so for example you can use $bi(level)$ for battery, or $100 / 60 * df(m)$ for minutes
* Finally set colors properly and test