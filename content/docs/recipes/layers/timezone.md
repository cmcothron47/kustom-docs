---
title: Show a clock with a different timezone
type: docs
categories:
  - Recipes
  - Layers
tags:
  - clock
  - timezone
---

# Multiple TZ clocks
In Kustom every layer can have a different location or timezone, changing this parameter affects all the items inside
the layer. If you change the location both weather, location, air quality and time data will change, if you just change
the timezone only the clock will be modified. You can change default locations in the app settings

## Tutorial
{{< col >}}

### Steps
First of all add a Widget (or a wallpaper or any other Kustom item)
- Create a normal Text Item and set it to a clock, like ```$df(hh:mm)$```, this will display local time
- Add a Layer, you can use a Stack Layer, an Overlap Group or a Komponent
- Add a Text Item to the layer, set formula again to ```$df(hh:mm)$```
- Go back to the Layer settings, set Location to one of the different ones or pick your Timezone
- You are done :)

<--->

### Video
{{< yt qt8pIYPrcCU "9:18" >}}

{{< /col >}}

