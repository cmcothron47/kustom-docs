---
title: Get title and image from a JSON API every hour
type: docs
categories:
- Recipes
- Flows
tags:
- flows
- images
- random
- wallpaper
---

# Get title and image from a JSON API every hour
Kustom Flows can chain events and store data into files that can be used later in formulas. In this example we will
get some data from an external API (the NASA daily picture API) and store the results locally, we will finally use
that data to display the image and its title

## Tutorial
{{< col >}}

### Steps
First of all add a Widget (or a wallpaper or any other Kustom item), then:
- Create a text that will hold the title
- Create a picture for the daily picture
- Go back to the Layer and add a Text global, call it "json"
- Go to Flows, add a new Flow
- Add an "on load trigger" this will trigger the flow once preset is loaded
- Add a "cron" trigger, with standard format 0 0 * * * it will trigger at every hour at the hour
- Add an Action of type WebGet, as an URI here i am using "https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY"
- Add an action to store to a Global, Flows will download the JSON file and assign to the global the local JSON file path
- Finally go back to the text and enter $wg(gv(json), json, .title)$ to show the title from the json
- Then go to the image, turn Bitmap parameter to  a formul and use $wg(gv(json), json, .url)$ to show the image
- You are done

<--->

### Video
{{< yt kWq56OoF0lM "9:18" >}}

{{< /col >}}


