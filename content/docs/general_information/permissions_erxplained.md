---
title: Permissions Explained
type: docs
categories:
  - General Information
tags:
  - General Information
  - Permissions
---

# Permissions Explained

## External Storage

Is used to cache content, store basic fonts and images and to export / import templates in the SD card, this is the only mandatory permission, without this Kustom will not work properly

## Location precise and network based

Are used to get the position for Weather and display information like street, city and country. Location is also used to calculate sunset and sunrise time, this is the main reason why its actually required since a lot of small things depends on Astronomy info. Providing location permission **is not mandatory** you can just select Manual when requested to provide the permission in the intro screen or setup a fixed location in the settings and then remove the permission from Android Settings.

## Network access

For weather information and to download custom content from the web. This permission is granted by default however is not mandatory, you can deny this permission from Android Settings and Kustom will work fine without it (off course network related functionalities wont work).

## Read Phone Status And Identity

Kustom needs this to show cellular information like signal, current operator name and connection type. This permission is normally requested **only** if you are actually using any of this functions in your preset.

## Pair BT devices

Is needed to get informations about the state of Bluetooth (on / off) and about paired devices, the permission will be requested ONLY when this functionality is being used

## Read Notifications

When you want to control a Music Player or to want to read Notifications text and counters custom will ask you to enable Notification access. While this might be obvious when reading Notifications text it might not for Media Players but unfortunately this is the only way to control them since the status of a Music Player is detected by its presence in the Notification area and without access to it Kustom cannot know when the player is started.

## Set Wallpaper

Is used to set Kustom as wallpaper, this is only used by KLWP