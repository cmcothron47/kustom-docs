---
title: Kustom Weather Beta
type: docs
categories:
  - General Information
tags:
  - General Information
  - weather
  - beta
---

# Kustom Weather Beta

Kustom Weather is currently in beta, in order to use it you have to:

* Ensure you are a beta tester of either KLWP or KWGT
* Join the beta channel at [https://kustom.rocks/weather/beta](https://kustom.rocks/weather/beta)
* Wait for a few minutes than go to the market page at [https://play.google.com/store/apps/details?id=org.kustom.weather](https://play.google.com/store/apps/details?id=org.kustom.weather)
* Install it, then go to KWGT/KLWP and press Weather Source it should now list the new providers
* When you select Weather Underground you will be allowed to enter your own API key or subscribe
* Dark Sky currently supports only subscription