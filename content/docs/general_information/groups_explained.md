---
title: Groups explained (Overlap, Stack, Komponent)
type: docs
categories:
  - General Information
tags:
  - General Information
  - groups
  - overlap
  - stack
  - komponent
---

# Groups explained (Overlap, Stack, Komponent)

### Overlap Group

The "Overlap" group is a container for other modules, the layer size will increase based on its contents so it will basically "wrap" the items inside it. As the name suggest the "Overlap" group will place the items one on top of the other, you will then need to use the padding or the anchor to move them inside, the anchor is relative to the Group so if you anchor something "top left" in an Overlap Group it will be the top left of the group itself.   
  
Benefits of using an Overlap Group are:

* You can scale all items inside the group together by using the Layer -> Scale property
* On a Wallpaper root you can "repeat" or "mirror" the layer using the "Tiling" option, this is very useful in order to create pattern based wallpapers
* You can animate, move and re position all items together
* Overlap Group provides "static center" rotation, so, if you enable rotation of a group the group will rotate based on the group center, so the overlap group is the basis to create clock hands (see the clock tutorial in the [tutorials section](https://docs.kustom.rocks/docs/tutorials/))

### Stack Group

A stack group is a container that allows automatic "stacking" of the items, so if you want to have multiple text items placed one after the other horizontally or vertically you will use a Stack. This is especially good when you want to place one item just after another but the item size might change dynamically. The stack allows also to anchor items all to one side of the group, try playing with the stacking option in the "Layer" section.

### Komponent

The "Komponent" is a special Overlap Layer that can be exported and reused. The "Komponent" group is like a preset within a preset and allows you to create your own modules with their own [global variables](https://docs.kustom.rocks/docs/how_to/use_globals/). So, for example, if you create a Clock you can then export the Komponent and add this clock to other presets by loading it in one shot, the globals of the Komponent will become the settings of it so when using the module you will not see the objects inside (unless you unlock it) but just the basic settings. Komponents can also be distributed via APK. Please check the [tutorials](https://docs.kustom.rocks/docs/tutorials/) for more examples.