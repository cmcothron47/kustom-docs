---
title: Cracked / Free version of KWGT Pro or KLWP Pro APK
type: docs
categories:
  - General information
tags:
  - General information
  - cracked
  - piracy
  - free
---
# Cracked / Free version of KWGT Pro or KLWP Pro APK

**TL;DR: THERE IS NO NEED TO DOWNLOAD A CRACKED APK JUST DOWNLOAD THE ADS SUPPORTED ALWAYS FREE VERSION OF KUSTOM IN THE DOWNLOADS SECTION [AT THIS LINK](https://kustom.rocks/downloads)**

At Kustom industries we think that you should be able to create your own setup for free, this is why free version of Kustom doesn't limit you when creating a preset from scratch, this is always free and will always be BUT importing a preset created by someone else will require a PRO key. Developing an app requires A LOT of work and to keep this work ongoing i need some support, no subscription, no permanent fees, just a single payment for a lifetime license, unfortunately the small ads i am showing in the standard free version doesnt even cover basic costs.

Without your support app would have been abandoned or sold long time ago, this happened to a lot of great products like Zooper Widget. Your support is a way to say that you want to see Kustom live.

HOWEVER there might be situations where you cannot support Kustom development even if you would like to, maybe you are a student with no access to a payment channel or you are in a country where the pro key is not distributed or maybe you dont have access to any Android Market where Kustom is sold (Huawei, Google...). In that case you would normally look for a cracked / modded version of Kustom Widget Pro KEY APK or Kustom Live Wallpaper APK. If you are in this situation then please do not use a cracked version just use the ads supported version available in the [download page](https://kustom.rocks/downloads), that version has some annoying ads but will not restrict you in any way in using import or other features, its totally free while still giving me something back for developing the app.

Thanks!