---
title: Designer Tips
type: docs
categories:
  - General Information
tags:
  - General Information
  - tips
  - design
---

# Designer Tips

### Reduce non "simple" items in the root container to the minum

In Kustom the modules on the "root" container uses much more resources than the nested ones so the more you have the more memory the skin will use. Please consider using [groups](https://docs.kustom.rocks/docs/general_information/groups_explained/) when possible or when there is a logical connection between the items (so a clock a weather widget and so on), this will also make the skin more easy to edit. This however does not apply to simple shapes like non rounded rects and circles without gradients, those items will not use any resource so you can use as many as you want.

### Avoid "one second" updates in big objects

Unless strictly necessary avoid using things that update every second or more in very big objects since this will increase the draw time

### How size is calculated & scaled across devices?

As you can see by default shapes width and height can go up to "720", that number (if you don't use scaling as explained below) always represent the full width of a device in portrait mode. This means that your preset will always be scaled automatically across different phones, so, if your template covers half screen it will always cover half screen no matter which density or pixel width you have. Kustom will take care of scaling shapes, font and images to be crisp depending on the target density (off course, in case of images, source bitmap must be big enough).

### Use root and group scaling

Sometimes you might find the max width and max height of objects a bit limiting, in that case you can tune how that value is then "converted" into real pixels by setting the "root" layer scaling mode to proportional and increasing it, so, for example, setting it to 200 will make the size of any of your items double. You can also do this on a per group basis using [overlap groups](https://docs.kustom.rocks/docs/general_information/groups_explained/).