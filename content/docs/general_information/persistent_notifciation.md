---
title: Persistent Notification
type: docs
categories:
  - General Information
tags:
  - General Information
  - Notification
---

# Persistent Notification

Starting from version 3.37 Kustom might show a persistent notification on top like this:

![](https://s3.amazonaws.com/chd-data/files/2759/4142016444_d2d408d00c6f2709343368dcd82f722a_188/Capture.PNG)

### TL;DR I dont care, how to disable it?

To have the notification go away you have two options, just do ONE of the following:

* Remove Kustom from Android Battery optimization (instructions [HERE](https://docs.kustom.rocks/docs/how_to/use_globals/)), this is the preferred approach
* Long press on the notification and press the "info" icon (or go to Android settings -> Applications -> Kustom app), then select "Notifications" and turn them off.

### Why is that needed?

Apps build for Android Oreo (API 26) or later are [automatically battery optimized](https://developer.android.com/about/versions/oreo/android-8.0-changes#back-all), it means that they cannot run background services for a long time, starting from November 2018 Google doesnt allow new apps and apps updates to be built for versions older than Oreo and for this reason Kustom had to comply to this rule. Kustom needs to display a clock, needs to know when battery status changes to track its usage, needs to follow location updates for weather data and listen from other events coming from Tasker and other places. For this reason it has to be always on.

### Does removing battery optimization affect my battery usage?

No it does not, Kustom was using close to 0 resources when it was allowed to ignore battery optimization (because targeting old releases) and new version acts like the same. Allowing Kustom to bypass battery optimization will just allow it to run the way its expected to run, Kustom will completely shut down everything and just keep listening to basic stuff while your device is idle to ensure proper data is displayed on your screen as you like.

### I have other apps with widgets that do not need this, why?

There are two reasons why other apps might not need that notification, they might be old, and old apps are not affected by this change or they might not need a background service at all. For example an app just showing a calendar doesnt need to always stay in the background since it just needs to know when calendar changes, same goes for an app that just shows messages or needs to be updated every 20/30 minutes. Kustom is a clock and needs to be updated every single minute when screen is on.

### I dont see any notification icon

Kustom will show the icon only if needed, if you gave Kustom access to your notifications because you build a music player then in that case notification will not show up, same goes if you are using KLWP since KLWP is a live wallpaper and live wallpapers are not battery optimized by default same as the Launcher