---
title: SI - system info
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - si
  - system info
---
<h1>SI: system info (next alarm, uptime, device model, dark mode, orientation, rom…)</h1>

<h3>Syntax</h3>
si(type)
<h3>Arguments</h3>
<ul>
  <li><b>type</b>: Info type, see examples</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$df("EEE hh:mma", si(alarmd))$</b></td>
    <td>Next alarm formatted date (if set)</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(alarmt)$</b></td>
    <td>Next alarm as original text</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(alarmon)$</b></td>
    <td>Alarm switch, return 1 if alarm is set, 0 otherwise</td>
  </tr>
  <tr>
    <td width="250px"><b>Next alarm $tf(si(alarmd))$</b></td>
    <td>Time to next alarm (if set)</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(land)$</b></td>
    <td>Screen Orientation (gives 0 in portrait, 1 if landscape)</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(locked)$</b></td>
    <td>Device Lock (gives 1 if locked, 0 if not)</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(lmode)$</b></td>
    <td>Location mode, one of: &#10;OFF, &#10;SENSORS_ONLY, &#10;BATTERY_SAVING, &#10;HIGH_ACCURACY, &#10;UNKNOWN</td>
  </tr>
  <tr>
    <td width="250px"><b>Uptime: $tf(df(S) - df(S, si(boot)))$</b></td>
    <td>Time since boot (uptime)</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(volr)$</b></td>
    <td>Ringer volume (0 to 100)</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(vola)$</b></td>
    <td>Alarm volume (0 to 100)</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(ringer)$</b></td>
    <td>Ringer mode, one of:: &#10;NORMAL, &#10;SILENT, &#10;VIBRATE</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(darkmode)$</b></td>
    <td>Returns 1 if system dark mode is on</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(darkwp)$</b></td>
    <td>Returns 1 if current wallpaper prefers dark colors</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(powersave)$</b></td>
    <td>Returns 1 if system is in power save mode (saving battery)</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(wpcolor1)$</b></td>
    <td>Primary color of wallpaper if available, 0 otherwise</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(wpcolor2)$</b></td>
    <td>Secondary color of wallpaper if available, 0 otherwise</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(rwidth)$</b></td>
    <td>Width of root container (in kustom points)</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(rheight)$</b></td>
    <td>Height of root container (in kustom points)</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(rratio)$</b></td>
    <td>Aspect ratio of root container (in kustom points)</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(model)$</b></td>
    <td>Phone Model</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(man)$</b></td>
    <td>Phone Manufacturer</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(build)$</b></td>
    <td>ROM Name</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(aver)$</b></td>
    <td>Android version number</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(mindex)$</b></td>
    <td>Module index in its parent</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(mcount)$</b></td>
    <td>Current group module count</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(mindex, 1)$</b></td>
    <td>Module index in its parent's parent</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(lnchname)$</b></td>
    <td>Name of currently set Launcher</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(lnchpkg)$</b></td>
    <td>Pkg of currently set Launcher</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(pkgname, si(lnchpkg))$</b></td>
    <td>Give name of an app from a pkg name</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(pkgver)$</b></td>
    <td>Gives version name of pkg if specified, of kustom if not</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(pkgvern, si(lnchpkg))$</b></td>
    <td>Gives version number of pkg if specified, of kustom if not</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(system, screen_brightness)$</b></td>
    <td>Current screen brightness</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(swidth)$</b></td>
    <td>Width of screen in DPI</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(sheight)$</b></td>
    <td>Height of screen in DPI</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(sdpi)$</b></td>
    <td>DPI</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(sdensity)$</b></td>
    <td>Density (Pixel Per DPI)</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(skpi)$</b></td>
    <td>KPI (Pixels per Kustom Point)</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(wpzoomed)$</b></td>
    <td>Returns 1 if wallpaper is currently zoomed (ex: app drawer is open)</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(sysca1, 50)$</b></td>
    <td>System (MaterialYou) first accent color at 50% tone (0 is white, 100 is black)</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(sysca2, 80)$</b></td>
    <td>System (MaterialYou) third accent color at 80% tone</td>
  </tr>
  <tr>
    <td width="250px"><b>$si(syscn1, 20)$</b></td>
    <td>System (MaterialYou) first neutral color at 20% tone</td>
  </tr>
</table>
