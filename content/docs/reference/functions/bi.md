---
title: BI - battery info
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - bi
  - battery info
---
<h1>BI: battery info (level, voltage, temperature, time since charging…)</h1>

<h3>Syntax</h3>
bi(type, [date])
<h3>Arguments</h3>
<ul>
  <li><b>type</b>: Info type, see examples</li>
  <li><b>date</b>: Optional date for historical data up to 24 hours, you can use usual format so, r1h will give one hour ago, r30m 30 minutes ago and so on (see examples)</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$bi(level)$%</b></td>
    <td>Battery Level (in %)</td>
  </tr>
  <tr>
    <td width="250px"><b>$bi(temp)$&#176;$wi(tempu)$</b></td>
    <td>Battery Temperature in local unit</td>
  </tr>
  <tr>
    <td width="250px"><b>$bi(tempc)$</b></td>
    <td>Battery Temperature in celsius</td>
  </tr>
  <tr>
    <td width="250px"><b>$bi(volt)$</b></td>
    <td>Battery voltage in millivolts</td>
  </tr>
  <tr>
    <td width="250px"><b>$bi(charging)$</b></td>
    <td>Will return 0 if on battery, 1 if charging</td>
  </tr>
  <tr>
    <td width="250px"><b>$bi(fast)$</b></td>
    <td>Will return 1 if fast charging, 0 otherwise (Android 5.x or better only)</td>
  </tr>
  <tr>
    <td width="250px"><b>$bi(source)$</b></td>
    <td>Current power source (Battery, AC, USB or Wireless)</td>
  </tr>
  <tr>
    <td width="250px"><b>$df("hh:mma", bi(plugged))$</b></td>
    <td>Date of last plugged / unplugged event</td>
  </tr>
  <tr>
    <td width="250px"><b>$bi(current)$</b></td>
    <td>Realtime charging/discharging current in milliampere</td>
  </tr>
  <tr>
    <td width="250px"><b>$if(bi(charging) = 0, "unplugged", "plugged")$ $tf(bi(plugged))$</b></td>
    <td>Time since last plugged / unplugged event</td>
  </tr>
  <tr>
    <td width="250px"><b>$df("hh:mma", bi(fullempty))$</b></td>
    <td>Date of expected next charged/discharged event</td>
  </tr>
  <tr>
    <td width="250px"><b>$if(bi(charging) = 0, discharged, charged)$ $tf(bi(fullempty))$</b></td>
    <td>Time to next expected charged/discharged event</td>
  </tr>
  <tr>
    <td width="250px"><b>$if(bi(charging) = 0, Discharged, Full)$$if(bi(charging) = 0 | bi(level) &lt; 100, " in " + tf(bi(fullempty) - dp()))$</b></td>
    <td>Alternate time to next expected charged/discharged event with relative time</td>
  </tr>
  <tr>
    <td width="250px"><b>$bi(level, r30m)$%</b></td>
    <td>Battery Level (in %) 30 minutes ago</td>
  </tr>
  <tr>
    <td width="250px"><b>$bi(source, r1h)$</b></td>
    <td>Battery Source 1 hour ago</td>
  </tr>
  <tr>
    <td width="250px"><b>$bi(temp, r2h)$&#176;$wi(tempu)$</b></td>
    <td>Battery Temp 2 hours ago</td>
  </tr>
</table>
