---
title: TU - timer utilities
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - tu
  - timer utilities
---
<h1>TU: timer utilities (pick number, image or file every minute, hour…)</h1>

<h3>Syntax</h3>
tu(mode, [timer])
<h3>Arguments</h3>
<ul>
  <li><b>mode</b>: Mode (see examples)</li>
  <li><b>timer</b>: Timer (in minutes, can be a fraction like 0.1)</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$tu(rnd, 1, 10, 20)$</b></td>
    <td>Return a random number between 10 and 20 every minute</td>
  </tr>
  <tr>
    <td width="250px"><b>$tu(seq, 1/4, 1, 100)$</b></td>
    <td>Goes from 1 to 100 (included) changing every 15 seconds</td>
  </tr>
  <tr>
    <td width="250px"><b>$tu(rndimg, 15, "/sdcard/pictures")$</b></td>
    <td>Pick a random image file from directory every 15 minutes</td>
  </tr>
  <tr>
    <td width="250px"><b>$tu(rndimg, 5, "/sdcard/pictures", "txt")$</b></td>
    <td>Pick a random image file matching pattern every 5 minutes</td>
  </tr>
  <tr>
    <td width="250px"><b>$tu(rndfile, 15, "/sdcard/foo")$</b></td>
    <td>Pick a random file from directory every 15 minutes</td>
  </tr>
  <tr>
    <td width="250px"><b>$tu(rndfile, 5, "/sdcard/foo", "txt")$</b></td>
    <td>Pick a random file matching pattern every 5 minutes</td>
  </tr>
</table>
