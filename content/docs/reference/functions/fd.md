---
title: FD - fitness data
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - fd
  - fitness data
---
<h1>FD: fitness data (activities data, total steps, calories…)</h1>

<h3>Syntax</h3>
fd(type, [start], [end], [activity/segment], [segment])
<h3>Arguments</h3>
<ul>
  <li><b>type</b>: Info type, see examples</li>
  <li><b>start</b>: Date to be used. The Date can be returned by some other function or you can use text. For text dates you can both set it statically using the format '1955y11M12d22h04m00s' to express year 1955, month 11, day 12 at 22:04:00 (all fields are optional), or use 'a/r' (add/remove) operators, so, for example 'a12m3s' will add 12 minutes and 3 secs to current date.</li>
  <li><b>end</b>: Date to be used. The Date can be returned by some other function or you can use text. For text dates you can both set it statically using the format '1955y11M12d22h04m00s' to express year 1955, month 11, day 12 at 22:04:00 (all fields are optional), or use 'a/r' (add/remove) operators, so, for example 'a12m3s' will add 12 minutes and 3 secs to current date.</li>
  <li><b>activity/segment</b>: Activity type, eg running, walking and so on (regexp are accepted), optionally you can put segment here</li>
  <li><b>segment</b>: Segment when filtering by activity (0 is the first segment the range, -1 the last)</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$fd(steps)$</b></td>
    <td>Steps today</td>
  </tr>
  <tr>
    <td width="250px"><b>$fd(cals)$</b></td>
    <td>Active calories today</td>
  </tr>
  <tr>
    <td width="250px"><b>$fd(calsr, a0d, a0d)$</b></td>
    <td>Inactive calories today</td>
  </tr>
  <tr>
    <td width="250px"><b>$fd(dista)$$fd(distu)$</b></td>
    <td>Distance today (in local unit)</td>
  </tr>
  <tr>
    <td width="250px"><b>$fd(dist)$</b></td>
    <td>Distance today (in meters)</td>
  </tr>
  <tr>
    <td width="250px"><b>$tf(fd(time))$</b></td>
    <td>Active time today</td>
  </tr>
  <tr>
    <td width="250px"><b>$fd(steps, r1d, r1d)$</b></td>
    <td>Steps yesterday</td>
  </tr>
  <tr>
    <td width="250px"><b>$fd(count)$</b></td>
    <td>Number of activities today</td>
  </tr>
  <tr>
    <td width="250px"><b>Last activity: $fd(activity, r1d, r0d, -1)$ for $tf(fd(time, r1d, r0d, -1))$ $df("hh:mma", fd(start, r1d, r0d, -1))$</b></td>
    <td>Last activity in the last 2 days</td>
  </tr>
  <tr>
    <td width="250px"><b>$fd(steps, 1w) / mu(abs, (tf(1w, D) + 1))$</b></td>
    <td>Average steps per day this week</td>
  </tr>
  <tr>
    <td width="250px"><b>Active for $tf(fd(time), H)$ hours and $tf(fd(time), m)$ minutes</b></td>
    <td>Active time today in hours and minutes</td>
  </tr>
  <tr>
    <td width="250px"><b>HR max:$fd(hrmax)$, min: $fd(hrmin)$, avg: $fd(hravg)$</b></td>
    <td>Max, min and average HR today</td>
  </tr>
  <tr>
    <td width="250px"><b>HR:$fd(hrmax, r60s, r0s)$</b></td>
    <td>Current HR if being recorded</td>
  </tr>
  <tr>
    <td width="250px"><b>Sleep:$tf(fd(sleept, 22hr1d, r0h))$</b></td>
    <td>Sleep time last night</td>
  </tr>
  <tr>
    <td width="250px"><b>Elevation:$fd(elema)$$fd(elemu)$</b></td>
    <td>Elevation gained today in local unit</td>
  </tr>
  <tr>
    <td width="250px"><b>Floors:$fd(floors)$</b></td>
    <td>Floors climbed today</td>
  </tr>
</table>
