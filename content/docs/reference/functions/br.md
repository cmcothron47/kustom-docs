---
title: BR - broadcast receiver
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - br
  - broadcast receiver
---
<h1>BR: broadcast receiver (get variables from third parties, es Tasker)</h1>

<h3>Syntax</h3>
br(source, var)
<h3>Arguments</h3>
<ul>
  <li><b>source</b>: Source name, eg tasker or zooper</li>
  <li><b>var</b>: Variable name, if the variable contains a formula the formula will be parsed</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$br(kwgt, FOOBAR)$</b></td>
    <td>Will write the value of variable FOOBAR sent from Kustom Widget Flow</td>
  </tr>
  <tr>
    <td width="250px"><b>$br(tasker, FOOBAR)$</b></td>
    <td>Will write the value of variable FOOBAR sent from Tasker action plugin</td>
  </tr>
  <tr>
    <td width="250px"><b>$br(zooper, FOOBAR)$</b></td>
    <td>Will write the value of variable FOOBAR sent to Zooper from Tasker or third party plugins (equivalent in Zooper would be #TFOOBAR#)</td>
  </tr>
  <tr>
    <td width="250px"><b>$br(remote, FOOBAR)$</b></td>
    <td>Will write the value of variable FOOBAR sent to Kustom via remote message (see remote token in advanced settings)</td>
  </tr>
</table>
