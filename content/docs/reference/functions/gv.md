---
title: GV - global variables
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - gv
  - global variables
---
<h1>GV: global variables (get value of a global variable)</h1>

<h3>Syntax</h3>
gv(var, [default], [index])
<h3>Arguments</h3>
<ul>
  <li><b>var</b>: The key of the global to retrieve</li>
  <li><b>default</b>: An optional default to return if global is not found</li>
  <li><b>index</b>: An optional index to return a specific item in list globals</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$gv(fgcolor, #FF0000)$</b></td>
    <td>Will return the value of var fgcolor or red color if not found</td>
  </tr>
</table>
