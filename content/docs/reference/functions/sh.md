---
title: SH - shell command
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - sh
  - shell command
---
<h1>SH: shell command (execute a command in the underlying shell)</h1>

<h3>Syntax</h3>
sh(cmd, [timeout], [lines])
<h3>Arguments</h3>
<ul>
  <li><b>cmd</b>: Command to execute</li>
  <li><b>timeout</b>: Refresh timeout in minutes or fractions (default 1 minute)</li>
  <li><b>lines</b>: Max lines to output (default 5)</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$sh("ps | grep '^u' | wc -l")$</b></td>
    <td>Number of user processes</td>
  </tr>
  <tr>
    <td width="250px"><b>$sh("cat /proc/cpuinfo | grep Hardware | sed 's/.*: //'")$</b></td>
    <td>Current CPU technology</td>
  </tr>
</table>
