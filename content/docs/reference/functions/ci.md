---
title: CI - calendar events
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - ci
  - calendar events
---
<h1>CI: calendar events (get title, time and data from multiple calendar events)</h1>

<h3>Syntax</h3>
ci(action, [index], [date], [calendar])
<h3>Arguments</h3>
<ul>
  <li><b>action</b>: Action (see examples)</li>
  <li><b>index</b>: Index of upcoming events if no date is provided, index of day events if date is added. When prefixed with an a will show only all day events, with e only normal ones. Index is zero based, 0 is the first event, 1 the second and so on, a0 is the first all day event, e0 the first non all day one.</li>
  <li><b>date</b>: Date to be used. The Date can be returned by some other function or you can use text. For text dates you can both set it statically using the format '1955y11M12d22h04m00s' to express year 1955, month 11, day 12 at 22:04:00 (all fields are optional), or use 'a/r' (add/remove) operators, so, for example 'a12m3s' will add 12 minutes and 3 secs to current date.</li>
  <li><b>calendar</b>: Override default Calendar selection</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$df(EEE hh:mm, ci(start, 0))$-$df(hh:mm, ci(end, 0))$ $ci(title, 0)$</b></td>
    <td>Day start-end title of first upcoming event</td>
  </tr>
  <tr>
    <td width="250px"><b>$ci(title, 1)$</b></td>
    <td>Title of second upcoming event</td>
  </tr>
  <tr>
    <td width="250px"><b>$ci(desc, 1)$</b></td>
    <td>Description of second upcoming event or all day event (the first)</td>
  </tr>
  <tr>
    <td width="250px"><b>$df("hh:mma", ci(start, 0))$</b></td>
    <td>Start date of next upcoming event in HH:MM format</td>
  </tr>
  <tr>
    <td width="250px"><b>$ci(allday, 0)$</b></td>
    <td>Will write 1 if next upcoming event is allday, 0 otherwise</td>
  </tr>
  <tr>
    <td width="250px"><b>$ci(title, a0, a0d)$</b></td>
    <td>Title of today's first all day event</td>
  </tr>
  <tr>
    <td width="250px"><b>$ci(desc, e1, a0d)$</b></td>
    <td>Description of today's second event</td>
  </tr>
  <tr>
    <td width="250px"><b>$ci(loc, e1, a0d)$</b></td>
    <td>Location of today's second event</td>
  </tr>
  <tr>
    <td width="250px"><b>$ci(ccolor, e0, a1d)$</b></td>
    <td>Calendar Color of first event tomorrow</td>
  </tr>
  <tr>
    <td width="250px"><b>$ci(cname, a1, a1d)$</b></td>
    <td>Calendar Name of first all day event tomorrow</td>
  </tr>
  <tr>
    <td width="250px"><b>$df("hh:mma", ci(end, 0, a0d))$</b></td>
    <td>End date of second event today in HH:MM format</td>
  </tr>
  <tr>
    <td width="250px"><b>$ci(color, 1, a1d)$</b></td>
    <td>Color of second event tomorrow</td>
  </tr>
  <tr>
    <td width="250px"><b>$ci(ecount, a0d)$</b></td>
    <td>Number of appointments today</td>
  </tr>
  <tr>
    <td width="250px"><b>$ci(acount, a1d)$</b></td>
    <td>Number of all day events tomorrow</td>
  </tr>
  <tr>
    <td width="250px"><b>$ci(url, 0)$</b></td>
    <td>Url of first event today (to be used in Touch, open Url action)</td>
  </tr>
  <tr>
    <td width="250px"><b>$ci(urld, 0)$</b></td>
    <td>Open calendar at first event time (to be used in Touch, open Url action)</td>
  </tr>
</table>
