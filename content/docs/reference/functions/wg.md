---
title: WG - web get
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - wg
  - web get
---
<h1>WG: web get (retrieve and parse text, rss and other content from HTTP links)</h1>

<h3>Syntax</h3>
wg([url], filter, params)
<h3>Arguments</h3>
<ul>
  <li><b>url</b>: Url to the http content</li>
  <li><b>filter</b>: Filter to use (RSS, TXT, XML, URL etc…)</li>
  <li><b>params</b>: Filter parameters (see examples)</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$wg("goo.gl/wNMV3f", txt)$</b></td>
    <td>Convert HTML content at URL into plain Text</td>
  </tr>
  <tr>
    <td width="250px"><b>$wg("quotes.rest/qod.xml", xml, "//quote")$</b></td>
    <td>Quote of the day text (parse XPath expression for XML content at URL)</td>
  </tr>
  <tr>
    <td width="250px"><b>$wg("quotes.rest/qod.xml", xml, "//author")$</b></td>
    <td>Quote of the day author (parse XPath expression for XML content at URL)</td>
  </tr>
  <tr>
    <td width="250px"><b>$wg("www.cnet.com/rss/news/", rss, title)$</b></td>
    <td>Get RSS feed title</td>
  </tr>
  <tr>
    <td width="250px"><b>$wg("cnet.com/rss/news/", rss, desc)$</b></td>
    <td>Get RSS feed description</td>
  </tr>
  <tr>
    <td width="250px"><b>$df("hh:mma", wg("cnet.com/rss/news/", rss, date))$</b></td>
    <td>Get RSS feed publish date</td>
  </tr>
  <tr>
    <td width="250px"><b>$wg("cnet.com/rss/news/", rss, count)$</b></td>
    <td>Get RSS feed entry count</td>
  </tr>
  <tr>
    <td width="250px"><b>$wg("cnet.com/rss/news/", rss, 0, title)$</b></td>
    <td>Get RSS feed title for entry 0</td>
  </tr>
  <tr>
    <td width="250px"><b>$wg("cnet.com/rss/news/", rss, 0, desc)$</b></td>
    <td>Get RSS feed content for entry 0</td>
  </tr>
  <tr>
    <td width="250px"><b>$wg("cnet.com/rss/news/", rss, 0, link)$</b></td>
    <td>Get RSS feed link for entry 0</td>
  </tr>
  <tr>
    <td width="250px"><b>$wg("cnet.com/rss/news/", rss, 0, thumb)$</b></td>
    <td>Get RSS feed first thumbnail image for entry 0</td>
  </tr>
  <tr>
    <td width="250px"><b>$df("hh:mma", wg("cnet.com/rss/news/", rss, 0, date))$</b></td>
    <td>Get RSS feed date for entry 0</td>
  </tr>
  <tr>
    <td width="250px"><b>$wg("500px.com/popular.rss", url, "cdn.500px.org")$</b></td>
    <td>Extract first URL matching the pattern</td>
  </tr>
  <tr>
    <td width="250px"><b>$wg("500px.com/popular.rss", url, "cdn.500px.org", count)$</b></td>
    <td>Number of URLs matching the pattern</td>
  </tr>
  <tr>
    <td width="250px"><b>$wg("500px.com/popular.rss", url, "cdn.500px.org", 3)$</b></td>
    <td>Extract third URL matching the pattern</td>
  </tr>
  <tr>
    <td width="250px"><b>$wg(jsonip.com, json, .ip)$</b></td>
    <td>Current IP via public service (parse JSONPath expression)</td>
  </tr>
  <tr>
    <td width="250px"><b>$wg("api.ipify.org/?format=json", reg, '[\{"\}]', 'X')$</b></td>
    <td>Search and replace from URL using Regular Expression</td>
  </tr>
  <tr>
    <td width="250px"><b>$wg("file:///sdcard/test.txt", raw)$</b></td>
    <td>Dump content of a text file in the SD without parsing</td>
  </tr>
  <tr>
    <td width="250px"><b>$wg("http://www.slashdot.org", jsoup, "meta[property=og:title]", content)$</b></td>
    <td>Read html meta og:title property content on element 0 using a JSoup selector</td>
  </tr>
</table>
