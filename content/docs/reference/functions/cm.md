---
title: CM - color maker
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - cm
  - color maker
---
<h1>CM: color maker (generate colors from ARGB/AHSV values)</h1>

<h3>Syntax</h3>
cm([a], r/h, g/s, b/v, [mode])
<h3>Arguments</h3>
<ul>
  <li><b>a</b>: Alpha value (optional, 0 is transparent 255 is fully opaque, default 255)</li>
  <li><b>r/h</b>: Red (0–255) / Hue (0–360)</li>
  <li><b>g/s</b>: Green (0–255) / Saturation (0–100)</li>
  <li><b>b/v</b>: Blue (0–255) / Value (0–100)</li>
  <li><b>mode</b>: Color mode (r for ARGB, h for AHSV, optional, default is ARGB)</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$cm(128,255,0,0)$</b></td>
    <td>Will generate a 50% transparent red color #FFFF0000</td>
  </tr>
  <tr>
    <td width="250px"><b>$cm(0,df(ss)*4.25,255-df(ss)*4.25)$</b></td>
    <td>Will move from blue to green based on seconds</td>
  </tr>
</table>
