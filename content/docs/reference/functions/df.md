---
title: DF - date format
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - df
  - date format
---
<h1>DF: date format (represent a time instant as text)</h1>

<h3>Syntax</h3>
df(format, [date])
<h3>Arguments</h3>
<ul>
  <li><b>format</b>: Format to be used for the date, see examples</li>
  <li><b>date</b>: Date to be used. The Date can be returned by some other function or you can use text. For text dates you can both set it statically using the format '1955y11M12d22h04m00s' to express year 1955, month 11, day 12 at 22:04:00 (all fields are optional), or use 'a/r' (add/remove) operators, so, for example 'a12m3s' will add 12 minutes and 3 secs to current date.</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$df(h:mm)$</b></td>
    <td>Hours and minutes with padding zero</td>
  </tr>
  <tr>
    <td width="250px"><b>$df(hh:mma)$</b></td>
    <td>Hours with leading zero, minutes and AM/PM marker (if 12h format in use)</td>
  </tr>
  <tr>
    <td width="250px"><b>$df(d MMM yyyy)$</b></td>
    <td>Current day number, month short name and full year</td>
  </tr>
  <tr>
    <td width="250px"><b>$df(dd/MM/yyyy)$</b></td>
    <td>Day / Month / Year numbers</td>
  </tr>
  <tr>
    <td width="250px"><b>$df(d)$$tc(ord, df(d))$</b></td>
    <td>Current day number with ordinal suffix</td>
  </tr>
  <tr>
    <td width="250px"><b>$df(hh)$</b></td>
    <td>Hours with padding zero</td>
  </tr>
  <tr>
    <td width="250px"><b>$df(mm)$</b></td>
    <td>Minutes with padding zero</td>
  </tr>
  <tr>
    <td width="250px"><b>$df(dd)$</b></td>
    <td>Day of the month with padding zero</td>
  </tr>
  <tr>
    <td width="250px"><b>$df(EEEE)$</b></td>
    <td>Current day name</td>
  </tr>
  <tr>
    <td width="250px"><b>$df(EEE, a1d)$</b></td>
    <td>Tomorrow's short day name</td>
  </tr>
  <tr>
    <td width="250px"><b>$df(MMMM)$</b></td>
    <td>Current month name</td>
  </tr>
  <tr>
    <td width="250px"><b>$df(D)$</b></td>
    <td>Day of year (number)</td>
  </tr>
  <tr>
    <td width="250px"><b>$df(w)$</b></td>
    <td>Week of year</td>
  </tr>
  <tr>
    <td width="250px"><b>$df(e)$</b></td>
    <td>Current day of the week (number, as per app settings)</td>
  </tr>
  <tr>
    <td width="250px"><b>$df(f)$</b></td>
    <td>ISO day of week (number, 1=Monday)</td>
  </tr>
</table>

<h3>Reference</h3>
<table>
  <tr>
    <th><b>Format</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td><b>h</b></td>
    <td>Hour of day (auto 1~12/0~23)</td>
  </tr>
  <tr>
    <td><b>hh</b></td>
    <td>Hour of day padded (1~12/0~23)</td>
  </tr>
  <tr>
    <td><b>m</b></td>
    <td>Minute of hour</td>
  </tr>
  <tr>
    <td><b>mm</b></td>
    <td>Minute of hour zero padded</td>
  </tr>
  <tr>
    <td><b>s</b></td>
    <td>Second of minute</td>
  </tr>
  <tr>
    <td><b>ss</b></td>
    <td>Second of minute zero padded</td>
  </tr>
  <tr>
    <td><b>a</b></td>
    <td>AM/PM marker (hidden in 24h)</td>
  </tr>
  <tr>
    <td><b>k</b></td>
    <td>Hour of day (auto 0~11/1~24)</td>
  </tr>
  <tr>
    <td><b>kk</b></td>
    <td>Hour of day padded (0~11/1~24)</td>
  </tr>
  <tr>
    <td><b>dd</b></td>
    <td>Day of month (number padded)</td>
  </tr>
  <tr>
    <td><b>M</b></td>
    <td>Month of year (number)</td>
  </tr>
  <tr>
    <td><b>MM</b></td>
    <td>Month of year (number padded)</td>
  </tr>
  <tr>
    <td><b>MMM</b></td>
    <td>Month of year (word short)</td>
  </tr>
  <tr>
    <td><b>MMMM</b></td>
    <td>Month of year (word long)</td>
  </tr>
  <tr>
    <td><b>E</b></td>
    <td>Day of week (word short)</td>
  </tr>
  <tr>
    <td><b>EEEE</b></td>
    <td>Day of week (word long)</td>
  </tr>
  <tr>
    <td><b>D</b></td>
    <td>Day of year (number)</td>
  </tr>
  <tr>
    <td><b>DDD</b></td>
    <td>Day of year (number padded)</td>
  </tr>
  <tr>
    <td><b>e</b></td>
    <td>Day of week (number)</td>
  </tr>
  <tr>
    <td><b>f</b></td>
    <td>ISO day of week (number, 1=Monday)</td>
  </tr>
  <tr>
    <td><b>F</b></td>
    <td>Week of Month</td>
  </tr>
  <tr>
    <td><b>o</b></td>
    <td>Days in current month (number 0-31)</td>
  </tr>
  <tr>
    <td><b>d</b></td>
    <td>Day of month (number)</td>
  </tr>
  <tr>
    <td><b>dd</b></td>
    <td>Day of month (number padded)</td>
  </tr>
  <tr>
    <td><b>A</b></td>
    <td>AM/PM marker (always visible)</td>
  </tr>
  <tr>
    <td><b>H</b></td>
    <td>Hour of day 0-23 (fixed)</td>
  </tr>
  <tr>
    <td><b>S</b></td>
    <td>Second since epoc (unix time)</td>
  </tr>
  <tr>
    <td><b>Z</b></td>
    <td>Time zone offset from GMT (in seconds)</td>
  </tr>
  <tr>
    <td><b>z</b></td>
    <td>Time zone indicator (es PST)</td>
  </tr>
  <tr>
    <td><b>W</b></td>
    <td>Time (hh:mm) as text</td>
  </tr>
  <tr>
    <td><b>zzzz</b></td>
    <td>Time zone description (es Pacific Standard Time)</td>
  </tr>
</table>