---
title: RM - resource monitor
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - rm
  - resource monitor
---
<h1>RM: resource monitor (cpu usage, memory, storage usage…)</h1>

<h3>Syntax</h3>
rm(type, [fs])
<h3>Arguments</h3>
<ul>
  <li><b>type</b>: Info type, see examples</li>
  <li><b>fs</b>: FS to get stats from, use int for internal, ext for SD (default) or specify custom absolute path</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$rm(cidle)$%</b></td>
    <td>Current idle cpu in %</td>
  </tr>
  <tr>
    <td width="250px"><b>$rm(cused)$%</b></td>
    <td>Current used (sys + usr) cpu in %</td>
  </tr>
  <tr>
    <td width="250px"><b>$rm(cusr)$%</b></td>
    <td>Current user cpu in %</td>
  </tr>
  <tr>
    <td width="250px"><b>$rm(csys)$%</b></td>
    <td>Current system cpu in %</td>
  </tr>
  <tr>
    <td width="250px"><b>$rm(fmin)$Mhz</b></td>
    <td>Min CPU frequency in Mhz</td>
  </tr>
  <tr>
    <td width="250px"><b>$rm(fmax)$Mhz</b></td>
    <td>Max CPU frequency in Mhz</td>
  </tr>
  <tr>
    <td width="250px"><b>$rm(fcur)$Mhz</b></td>
    <td>Current CPU frequency in Mhz</td>
  </tr>
  <tr>
    <td width="250px"><b>$rm(mtot)$MB</b></td>
    <td>Total memory in Mb</td>
  </tr>
  <tr>
    <td width="250px"><b>$rm(mfree)$MB</b></td>
    <td>Free memory in Mb</td>
  </tr>
  <tr>
    <td width="250px"><b>$rm(mused)$MB</b></td>
    <td>Used memory in Mb</td>
  </tr>
  <tr>
    <td width="250px"><b>$rm(fstot)$MB</b></td>
    <td>Total SD FS space in Mb</td>
  </tr>
  <tr>
    <td width="250px"><b>$rm(fsfree)$MB</b></td>
    <td>Free SD FS space in Mb</td>
  </tr>
  <tr>
    <td width="250px"><b>$rm(fsused)$MB</b></td>
    <td>Used SD FS space in Mb</td>
  </tr>
  <tr>
    <td width="250px"><b>$rm(fstot, int)$MB</b></td>
    <td>Total internal FS space in Mb</td>
  </tr>
  <tr>
    <td width="250px"><b>$rm(fsfree, int)$MB</b></td>
    <td>Free internal FS space in Mb</td>
  </tr>
  <tr>
    <td width="250px"><b>$rm(fsfree, "/sdcard/external_sd")$MB</b></td>
    <td>Free space of FS mounted in /sdcard/external_sd in Mb</td>
  </tr>
</table>
