---
title: MU - math utilities
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - mu
  - math utilities
---
<h1>MU: math utilities (floor, ceil, sqrt, min, max…)</h1>

<h3>Syntax</h3>
mu(var, [default])
<h3>Arguments</h3>
<ul>
  <li><b>var</b>: Function (one of ceil, floor or sqrt)</li>
  <li><b>default</b>: One or more values depending on the function</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$mu(ceil, 3.14)$</b></td>
    <td>Will return ceil of 3.14</td>
  </tr>
  <tr>
    <td width="250px"><b>$mu(floor, 3.80)$</b></td>
    <td>Will return floor of 3.80</td>
  </tr>
  <tr>
    <td width="250px"><b>$mu(sqrt, 2)$</b></td>
    <td>Will return square root of 2</td>
  </tr>
  <tr>
    <td width="250px"><b>$mu(round, 2.80)$</b></td>
    <td>Will round 2.80 to the nearest integer</td>
  </tr>
  <tr>
    <td width="250px"><b>$mu(round, 2.858284, 2)$</b></td>
    <td>Will round number to 2 decimals</td>
  </tr>
  <tr>
    <td width="250px"><b>$mu(min, 1, 3)$</b></td>
    <td>Will return min between 1 and 3</td>
  </tr>
  <tr>
    <td width="250px"><b>$mu(max, 1, 3)$</b></td>
    <td>Will return max between 1 and 3</td>
  </tr>
  <tr>
    <td width="250px"><b>$mu(abs, -1)$</b></td>
    <td>Will return absolute value of -1</td>
  </tr>
  <tr>
    <td width="250px"><b>$mu(cos, 90)$</b></td>
    <td>Cosine of 90 degrees</td>
  </tr>
  <tr>
    <td width="250px"><b>$mu(sin, 90)$</b></td>
    <td>Sine of 90 degrees</td>
  </tr>
  <tr>
    <td width="250px"><b>$mu(tan, 45)$</b></td>
    <td>Tangent of 45 degrees</td>
  </tr>
  <tr>
    <td width="250px"><b>$mu(acos, 1)$</b></td>
    <td>Inverse Cosine of 1</td>
  </tr>
  <tr>
    <td width="250px"><b>$mu(asin, 1)$</b></td>
    <td>Inverse Sine of 1</td>
  </tr>
  <tr>
    <td width="250px"><b>$mu(atan, 45)$</b></td>
    <td>Inverse Tangent of 45 degrees</td>
  </tr>
  <tr>
    <td width="250px"><b>$mu(log, 5)$</b></td>
    <td>Logarithm of 5</td>
  </tr>
  <tr>
    <td width="250px"><b>$mu(pow, 2, 3)$</b></td>
    <td>2 raised to the power of 3</td>
  </tr>
  <tr>
    <td width="250px"><b>$mu(ln, 5)$</b></td>
    <td>Natural logarithm of 5</td>
  </tr>
  <tr>
    <td width="250px"><b>$mu(rnd, 10, 100)$</b></td>
    <td>Random number between 10 and 100</td>
  </tr>
  <tr>
    <td width="250px"><b>$mu(h2d, 5F)$</b></td>
    <td>Converts an hex number to decimal</td>
  </tr>
  <tr>
    <td width="250px"><b>$mu(d2h, 123)$</b></td>
    <td>Converts a decimal number to hex</td>
  </tr>
</table>
