---
title: TS - traffic stats
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - ts
  - traffic stats
---
<h1>TS: traffic stats (current download / upload speed, data usage statistics…)</h1>

<h3>Syntax</h3>
ts(type, [unit], [start], [end])
<h3>Arguments</h3>
<ul>
  <li><b>type</b>: Data type (use trx for total bytes download and ttx for total uploaded, tt for total uploaded downloaded, mrx/mtx/mt for mobile only stats and wrx/wtx/wt for wifi only traffic)</li>
  <li><b>unit</b>: Unit, a for auto (default, will add unit), b for bytes, k for kilobytes or m for megabytes</li>
  <li><b>start</b>: Traffic stats start date</li>
  <li><b>end</b>: Traffic stats end date</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$ts(trx)$</b></td>
    <td>Current download speed in automatic unit</td>
  </tr>
  <tr>
    <td width="250px"><b>$ts(ttx)$</b></td>
    <td>Current upload speed in automatic unit</td>
  </tr>
  <tr>
    <td width="250px"><b>$ts(mt, a, r0d)$</b></td>
    <td>Total mobile traffic today</td>
  </tr>
  <tr>
    <td width="250px"><b>$ts(mt, a, r1d, r1d)$</b></td>
    <td>Total mobile traffic yesterday</td>
  </tr>
  <tr>
    <td width="250px"><b>$ts(mt, a, r1w)$</b></td>
    <td>Total mobile traffic in the last 7 days</td>
  </tr>
  <tr>
    <td width="250px"><b>$ts(mt, a, 2d)$</b></td>
    <td>Total mobile traffic from the second day of this month</td>
  </tr>
  <tr>
    <td width="250px"><b>$ts(mt, a, 1dr1M, 1dr1d)$</b></td>
    <td>Total mobile traffic last month (from the 1st to the last day of the month)</td>
  </tr>
</table>
