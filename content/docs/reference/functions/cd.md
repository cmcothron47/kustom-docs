---
title: CD - complication data
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - cd
  - complication data
---
<h1>CD: complication data (Get data from watch complications)</h1>

<h3>Syntax</h3>
cd(id, type)
<h3>Arguments</h3>
<ul>
  <li><b>id</b>: Complication ID, numerical, 0 is background complication</li>
  <li><b>type</b>: Field type to extract (see examples)</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$cd(1, stext)$</b></td>
    <td>First complication small text</td>
  </tr>
  <tr>
    <td width="250px"><b>$cd(2, ltext)$</b></td>
    <td>Second complication long text if available</td>
  </tr>
  <tr>
    <td width="250px"><b>$cd(3, icon)$</b></td>
    <td>Third complication icon</td>
  </tr>
  <tr>
    <td width="250px"><b>$cd(1, rval)$</b></td>
    <td>First complication ranged value</td>
  </tr>
  <tr>
    <td width="250px"><b>$cd(1, rmin)$</b></td>
    <td>First complication ranged min value</td>
  </tr>
  <tr>
    <td width="250px"><b>$cd(1, rmax)$</b></td>
    <td>First complication ranged max value</td>
  </tr>
  <tr>
    <td width="250px"><b>$cd(1, simg)$</b></td>
    <td>First complication small image</td>
  </tr>
  <tr>
    <td width="250px"><b>$cd(0, limg)$</b></td>
    <td>Background complication large image</td>
  </tr>
  <tr>
    <td width="250px"><b>$cd(0, title)$</b></td>
    <td>First complication title if available</td>
  </tr>
</table>
