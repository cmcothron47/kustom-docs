---
title: NC - network connectivity
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - nc
  - network connectivity
---
<h1>NC: network connectivity (wifi / phone signal, operator name, network state…)</h1>

<h3>Syntax</h3>
nc(text)
<h3>Arguments</h3>
<ul>
  <li><b>text</b>: Info type, see examples and reference</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$nc(csig)$</b></td>
    <td>Cell signal from 0 to 4</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(operator)$</b></td>
    <td>Current cell operator</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(dtype)$</b></td>
    <td>Current cellular data connection type (LTE, HSUPA&#8230;)</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(dtypes)$</b></td>
    <td>Current cellular data connection short type (4G, 3G&#8230;)</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(ssid)$</b></td>
    <td>Current WiFi SSID (if connected)</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(wsig)$</b></td>
    <td>Wifi signal from 0 to 9</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(csiga)$</b></td>
    <td>Cell signal level as an asu value between 0..31, 99 is unknown</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(csigd)$</b></td>
    <td>Cell signal level in dbm</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(wrssi)$</b></td>
    <td>Wifi signal raw (RSSI)</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(wspeed)$</b></td>
    <td>Wifi speed in Megabit</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(bt)$</b></td>
    <td>Current BlueTooth static, 0 disabled, 1 enabled, 2 connected</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(acount)$</b></td>
    <td>Count of currently connected Audio Devices</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(aname, 0)$</b></td>
    <td>Name of the first audio device</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(aaddr, 0)$</b></td>
    <td>Address of the first BT audio device</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(abatt, 0)$</b></td>
    <td>Battery of the first BT audio device (when supported)</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(airplane)$</b></td>
    <td>Airplane mode, 0 if disabled, 1 if enabled</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(simcount)$</b></td>
    <td>Get current number of active SIMs</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(ifip)$</b></td>
    <td>Returns IPv4 of first non loopback interface</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(ifname)$</b></td>
    <td>Returns name of first non loopback interface</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(ifip, 1)$</b></td>
    <td>Returns IPv4 of second non loopback interface (if present)</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(cid)$</b></td>
    <td>Cell ID (CID) from network operator</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(lac)$</b></td>
    <td>Location Area Code (LAC) from network operator</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(carrier, 0)$</b></td>
    <td>Get SIM operator name for first SIM</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(csig, 1)$</b></td>
    <td>Cell signal for second sim</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(operator, 1)$</b></td>
    <td>Current cell operator for second sim</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(operator, nc(svoice))$</b></td>
    <td>Default SIM Operator for Voice</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(operator, nc(sdata))$</b></td>
    <td>Default SIM Operator for Data</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(operator, 1)$</b></td>
    <td>Current cell operator for second sim</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(cell)$</b></td>
    <td>Current cellular status, one of:: &#10;OFF, &#10;AIRPLANE, &#10;ON, &#10;DATA, &#10;ROAMING, &#10;DATAROAMING</td>
  </tr>
  <tr>
    <td width="250px"><b>$nc(wifi)$</b></td>
    <td>Current WiFi status, one of:: &#10;DISABLED, &#10;ENABLED, &#10;CONNECTED</td>
  </tr>
</table>
