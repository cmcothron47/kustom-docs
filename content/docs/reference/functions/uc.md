---
title: UC - unread counters
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - uc
  - unread counters
---
<h1>UC: unread counters (get number of unread SMS, missed calls, emails…)</h1>

<h3>Syntax</h3>
uc(mode, [param], [account])
<h3>Arguments</h3>
<ul>
  <li><b>mode</b>: Mode (see examples)</li>
  <li><b>param</b>: Parameter (see examples)</li>
  <li><b>account</b>: Account (index or pattern, when available)</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$uc(sms)$</b></td>
    <td>Unread SMS count</td>
  </tr>
  <tr>
    <td width="250px"><b>$uc(sms, text)$</b></td>
    <td>First unread SMS text</td>
  </tr>
  <tr>
    <td width="250px"><b>$tf(uc(sms, date, 1))$</b></td>
    <td>Second unread SMS receive date</td>
  </tr>
  <tr>
    <td width="250px"><b>$uc(sms, from, 2)$</b></td>
    <td>Third unread SMS sender number</td>
  </tr>
  <tr>
    <td width="250px"><b>$uc(calls)$</b></td>
    <td>Missed calls count</td>
  </tr>
  <tr>
    <td width="250px"><b>$uc(calls, num)$</b></td>
    <td>Last missed call number</td>
  </tr>
  <tr>
    <td width="250px"><b>$uc(calls, name)$</b></td>
    <td>Last missed call name (if available, number otherwise)</td>
  </tr>
  <tr>
    <td width="250px"><b>$tf(uc(calls, date))$</b></td>
    <td>Time since last missed call</td>
  </tr>
  <tr>
    <td width="250px"><b>$uc(gmail)$</b></td>
    <td>Gmail unread count for Inbox</td>
  </tr>
  <tr>
    <td width="250px"><b>$uc(gmail, forums)$</b></td>
    <td>Gmail unread count for Forums label</td>
  </tr>
  <tr>
    <td width="250px"><b>$uc(gmail, promo, color)$</b></td>
    <td>Gmail Promotions label color</td>
  </tr>
  <tr>
    <td width="250px"><b>$uc(gmail, updates, count, foo)$</b></td>
    <td>Gmail total messages on Updates label in foo account</td>
  </tr>
  <tr>
    <td width="250px"><b>$uc(gmail, social, unread, 1)$</b></td>
    <td>Gmail unread messages on Social label in second account</td>
  </tr>
  <tr>
    <td width="250px"><b>$uc(whatsapp)$</b></td>
    <td>Unread WhatsApp conversations</td>
  </tr>
  <tr>
    <td width="250px"><b>$tf(uc(whatsapp, date, 1))$</b></td>
    <td>Second unread WhatsApp receive date</td>
  </tr>
  <tr>
    <td width="250px"><b>$uc(whatsapp, from, 2)$</b></td>
    <td>Third unread WhatsApp sender</td>
  </tr>
</table>
