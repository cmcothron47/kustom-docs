---
title: CE - color editor
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - ce
  - color editor
---
<h1>CE: color editor (manipulates ARGB or HSV color values)</h1>

<h3>Syntax</h3>
ce(color, filter, [amount])
<h3>Arguments</h3>
<ul>
  <li><b>color</b>: A valid ARGB or RGB color String (eg #FF663399)</li>
  <li><b>filter</b>: Filter (alpha opacity, sat saturation, lum luminance) or gradient end color (see examples)</li>
  <li><b>amount</b>: A value between 0 and 100, for alpha 0 is fully transparent, for saturation 0 means greyscale and for luminance 0 is black</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$ce(#FF0000, invert)$</b></td>
    <td>Will invert RGB color</td>
  </tr>
  <tr>
    <td width="250px"><b>$ce(#FF0000, comp)$</b></td>
    <td>Will return complementary color</td>
  </tr>
  <tr>
    <td width="250px"><b>$ce(#FF0000, contrast)$</b></td>
    <td>Return either black or white depending on best contrast</td>
  </tr>
  <tr>
    <td width="250px"><b>$ce(#FF0000, alpha, 50)$</b></td>
    <td>Will make a fully opaque red into 50% transparent</td>
  </tr>
  <tr>
    <td width="250px"><b>$ce(#FF0000, sat, 0)$</b></td>
    <td>Will convert red color into Greyscale equivalent</td>
  </tr>
  <tr>
    <td width="250px"><b>$ce(#FF0000, lum, 50)$</b></td>
    <td>Will set red luminance to 50</td>
  </tr>
  <tr>
    <td width="250px"><b>$ce(#FF0000, lum, a50)$</b></td>
    <td>Will add 50 to red luminance (0 to 100)</td>
  </tr>
  <tr>
    <td width="250px"><b>$ce(#FF0000, alpha, r50)$</b></td>
    <td>Will remove 50 from red alpha (0 to 255)</td>
  </tr>
  <tr>
    <td width="250px"><b>$ce(#FF0000, #FF0000, 50)$</b></td>
    <td>Mix half red and half blue (50% gradient)</td>
  </tr>
</table>
