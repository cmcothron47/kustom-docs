---
title: AQ - air quality
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - aq
  - air quality
---
<h1>AQ: air quality (pollution index, co2, pm10…)</h1>

<h3>Syntax</h3>
aq(type)
<h3>Arguments</h3>
<ul>
  <li><b>type</b>: Info type, see examples</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$aq(index)$</b></td>
    <td>Air quality index (0 = best, 400 = worst)</td>
  </tr>
  <tr>
    <td width="250px"><b>$aq(label)$</b></td>
    <td>Air quality level label</td>
  </tr>
  <tr>
    <td width="250px"><b>$aq(level)$</b></td>
    <td>Air quality level, one of:: &#10;NA, &#10;GOOD, &#10;MODERATE, &#10;UNHEALTHY_FOR_SENSITIVE, &#10;UNHEALTHY, &#10;VERY_UNHEALTHY, &#10;HAZARDOUS</td>
  </tr>
  <tr>
    <td width="250px"><b>$aq(no2)$</b></td>
    <td>Average No2 (&#181;g/m&#179;)</td>
  </tr>
  <tr>
    <td width="250px"><b>$aq(pm10)$</b></td>
    <td>Average PM10 (&#181;g/m&#179;)</td>
  </tr>
  <tr>
    <td width="250px"><b>$aq(pm25)$</b></td>
    <td>Average PM25 (&#181;g/m&#179;)</td>
  </tr>
  <tr>
    <td width="250px"><b>$aq(station)$</b></td>
    <td>Data station ID</td>
  </tr>
  <tr>
    <td width="250px"><b>$aq(source)$</b></td>
    <td>Name of the data source</td>
  </tr>
  <tr>
    <td width="250px"><b>$tf(aq(updated))$</b></td>
    <td>Last data check in hh:mm format</td>
  </tr>
  <tr>
    <td width="250px"><b>$tf(aq(collected))$</b></td>
    <td>Time of data measurement in hh:mm format</td>
  </tr>
</table>
