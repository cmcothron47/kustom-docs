---
title: Functions
bookCollapseSection: true
type: docs
weight: 100
---
# Kustom Functions
Kustom employs simple functions to display text or information in text form, visible in the formula editor. Mistakes in basic stuff are flagged, and a text preview is always provided. Examples of these functions include basic mathematical operations like addition, subtraction, multiplication, etc.

### Syntax
Formulas are mostly connected functions. They are enclosed by `$` symbols, which signify the start and end of a formula. Math operators such as `+`, `-`, `/`, etc., can be used within these formulas. The way these operators work varies according to their usage. For instance, they can perform standard math, concatenate strings, or manipulate date/time values. More info on mathematical operators can be found in 
the [Math Operators]({{< ref "/docs/more/math_operators.md" >}}) page.

Conditional operators are frequently used in Kustom Formulas. These include:

- `"="` for equals
- `">"` for greater than
- `">="` for greater than or equal to
- `"<"` for less than
- `"<="` for less than or equal to
- `"&"` for AND
- `"|"` for OR

Parentheses can also be used for structuring and precedence in formulas. 

When you see a calculator icon in the Kustom application, it indicates that you can use a formula there. It's essential to note that Kustom also provides rules about what the formula needs to return. 

### Available Functions
{{< series "functions/" >}}
