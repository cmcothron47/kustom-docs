---
title: DP - date parser
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - dp
  - date parser
---
<h1>DP: date parser (Creates a date or a time span from text)</h1>

<h3>Syntax</h3>
dp(date, [pattern])
<h3>Arguments</h3>
<ul>
  <li><b>date</b>: Date to be used. The Date can be returned by some other function or you can use text. For text dates you can both set it statically using the format '1955y11M12d22h04m00s' to express year 1955, month 11, day 12 at 22:04:00 (all fields are optional), or use 'a/r' (add/remove) operators, so, for example 'a12m3s' will add 12 minutes and 3 secs to current date.</li>
  <li><b>pattern</b>: A standard Java Date Time format pattern to parse the date</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$dp(0h0m0s)$</b></td>
    <td>Today's midnight</td>
  </tr>
  <tr>
    <td width="250px"><b>$dp(01M01d0h0m0sa1y)$</b></td>
    <td>New year's eve</td>
  </tr>
  <tr>
    <td width="250px"><b>$dp("2010-07-30T16:03:25Z", auto)$</b></td>
    <td>Parse date string using ISO / auto format</td>
  </tr>
  <tr>
    <td width="250px"><b>$dp(05-07-2010, dd-MM-yyyy)$</b></td>
    <td>Parse date string using custom date format</td>
  </tr>
</table>
