---
title: FL - for loops
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - fl
  - for loops
---
<h1>FL: for loops (execute a statement multiple times)</h1>

<h3>Syntax</h3>
fl(init, stop, increment, loop, [sep])
<h3>Arguments</h3>
<ul>
  <li><b>init</b>: First value of the i variable</li>
  <li><b>stop</b>: Condition valuue to meet in order to stop, eg 10</li>
  <li><b>increment</b>: Increment formula, eg i + 1</li>
  <li><b>loop</b>: Repeat formula or value, i will be replaced with the var</li>
  <li><b>sep</b>: Optional separator between elements</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$fl(5, 20, "i + 1", "i", " ")$</b></td>
    <td>Write numbers from 5 to 20 spaced</td>
  </tr>
  <tr>
    <td width="250px"><b>$fl(1, 7, "i + 1", "df(EEE, a + i + d)")$</b></td>
    <td>Write short name of next 7 days</td>
  </tr>
  <tr>
    <td width="250px"><b>$fl(1, tu(seq, 1, 1, 10), "i + 1", "#")$</b></td>
    <td>Shows from 1 to 10 # symbols, adding one every minute</td>
  </tr>
</table>
