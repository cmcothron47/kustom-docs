---
title: TF - time span
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - tf
  - time span
---
<h1>TF: time span (format a relative time span, like 3 hours ago or in 3 days))</h1>

<h3>Syntax</h3>
tf(date, [format])
<h3>Arguments</h3>
<ul>
  <li><b>date</b>: Date to be used. The Date can be returned by some other function or you can use text. For text dates you can both set it statically using the format '1955y11M12d22h04m00s' to express year 1955, month 11, day 12 at 22:04:00 (all fields are optional), or use 'a/r' (add/remove) operators, so, for example 'a12m3s' will add 12 minutes and 3 secs to current date.</li>
  <li><b>format</b>: Optional format to be used for the time, see examples</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$tf(bi(plugged))$</b></td>
    <td>Time since last battery plugged/unplugged</td>
  </tr>
  <tr>
    <td width="250px"><b>Midnight $tf(0h0m0sa1d)$</b></td>
    <td>Time to midnight (we first set the time to 0 hours, 0 mins and 0 sec then we add one day at the end)</td>
  </tr>
  <tr>
    <td width="250px"><b>Midnight in $tf(0h0m0sa1d, hh:mm:ss)$</b></td>
    <td>Countdown to midnight (same as before but we use custom format)</td>
  </tr>
  <tr>
    <td width="250px"><b>$tf(ai(sunset) - ai(sunrise))$</b></td>
    <td>Current duration of daylight, automatic format</td>
  </tr>
  <tr>
    <td width="250px"><b>Tonight $tf(ai(sunrise, a1d) - ai(sunset), "h' hours' and m' minutes'")$ of darkness</b></td>
    <td>Darkness duration, manual format</td>
  </tr>
  <tr>
    <td width="250px"><b>Sunrise in $tf(ai(nsunrise), M)$ minutes</b></td>
    <td>Minutes till next sunrise</td>
  </tr>
</table>
