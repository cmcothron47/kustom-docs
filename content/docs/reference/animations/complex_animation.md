---
title: Complex Animation
type: docs
categories:
  - General Information
tags:
  - animation
  - advanced
---

# Complex Animation

A complex animation is similar to CSS3, within any point of the transition from 0% (start) to 100% (end) you can have a property set to a value, Kustom will then move smoothly to that. So if you say that X should be at 100 at position 50 Kustom will move by 100 in the first half of the animation then stop.

Lets start with an example, lets say you want to:

* Move down 500 Kustom Pixels
* AFTER the movement, you want the image to scale up 3 times

To do so we add a Complex animation and we add the following:

* At 50% we state that we want Y offset to be 500, this will make Kustom go from 0 to 500 in the first half of the animation, it will then stop
* At 50% we also state that XY Scaling is 1, this will tell Kustom that we are not changing the Scale in the first half of the animation, so scale will not be touched
* At 100% we state that XY Scaling is 3, so Kustom will start scaling from 1 at 50% of the animation and go up to 3 at the end

Done! Risult will be something like this:

![Complex Animation](https://giphy.com/static/img/gif-404.gif)
