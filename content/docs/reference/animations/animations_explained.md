---
title: Animations explained (rule, speed, amount...)
type: docs
categories:
  - General Information
tags:
  - animations
  - rules
---

# Animations explained (rule, speed, amount...)

In Kustom all items placed in the "root" module can be animated, in order to do so please use the "animation" tab and add one or more animations. If more than one animation is added the animations will be combined, the order is not relevant.   
  
Animations have a lot of parameters, let's see what they do:

* **ReactOn** this will control when the animation will be triggered, currently only "scroll" is supported which means that it will be activated by screen swiping
* **Action** represents the type of animation to perform, scroll and scroll inverted will move the object horizontally, fade will change its transparency, scale will change the size and rotate will rotate the object
* **Center** decides in which screen the item will be in its "starting" position so this is where the animation will start from, for example, if you have a centered item and center set to screen 1 the item will be centered on screen 1 and start moving from there
* **Speed** controls how fast the "action" is performed, for example when fading at speed 100 in scroll the item will go from full opacity to fully transparent in one screen, if speed is 50 it will take 2 screens to fade out
* **Amount** will decide when to stop, 100 means that the full animation will be applied (so, in case of fade it will go from full opacity to transparent), 50 means that only 50% will be applied, so when opacity will be 50% it will stop. This is available only on certain animations.
* **Rule** controls when this animation is applied, so for example "before center" will tell the animator to use the animation only up to the center and ignore it after it. The default is "center" which means that animation will be applied before and after the center.
* **Anchor** will set the anchor point for a rotation or a scaling
