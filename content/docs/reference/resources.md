---
title: Resources (kfile)
type: docs
categories:
  - Reference
tags:
  - kfile
  - uri
  - advanced
  - resources
---
# Kustom resources

As you might notice when you start using formulas for Bitmaps or other type of objects you might realize that Kustom 
refers internally to resources using the form ```kfile://provider/path```, this is the internal URL scheme for looking
up local and remote files.

The provider represents the "source" and can be of two different types:

  - Relative: when source is not known and Kustom has to look it up, a relative provider is always
"org.kustom.provider", this is the default when you export a preset and it will tell Kustom to search all available 
sources for a given preset and search for "path"
  - Absolute: when source is known, this can generaly be 
    - A provider from an APK pack (their content provider)
    - Kustom own provider (so for KWGT org.kustom.widget.provider)
    - The SD card provider (starts with SDxx)
    - The internal storage provider (starts with storage)

Please never hardcode a provider in a KFile, always use the relative path if you need to reference files manually
