---
title: Flows
type: docs
categories:
  - Reference
tags:
  - functions
  - flows
  - tasker
  - gv
---

# Flows

From Kustom 3.70 all layers have a new tab called "flows", what are flows? A flow is a quick way to get one or more
tasks done with your apps. The Flows tab app lets you create your own automations with multiple steps. For example,
download some data from a site then parse the content and download an image from it, or generate some text every hour
or trigger an animation through a global with some delay after a touch.

## What’s a trigger?

A trigger is what starts the Flow, you can for example use a "manual" trigger to start it only associated with a touch
shortcut, or use "cron" to run the flow at specific time of the day, you can also trigger based on formula outputs or
periodic timers. Triggers can be combined and used together.

## What’s an action?

An action — the building block of a flow — is a single step in a task. Mix and match actions to create flows
that interact content and services on the internet or globals. Use Local variables to store temporary values that you
need to parse right away in formulas.

## Examples

Here is a few flow recipes:

- [Load an Image on Click]({{< ref "/docs/recipes/flows/imageonclick.md" >}})
- [Download JSON]({{< ref "/docs/recipes/flows/jsonimage.md" >}}) 
- [Random Unsplash Image on Click]({{< ref "/docs/recipes/flows/unsplash.md" >}})
