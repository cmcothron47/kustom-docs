---
title: Dynamic colors
type: docs
categories: [Reference]
tags: [material, colors, wallpaper, theming]
---

# System colors

All Kustom apps can extract color palette from different sources, such as:

-   Wallpaper through the [System Info](/docs/reference/functions/si) function: `$si(wpcolor1)`
-   A given image using the [Bitmap Palette](/docs/reference/functions/bp) function: `$bp(img, 1)`
-   System palette either based on wallpaper or
    via [Material You](https://m3.material.io/styles/color/overview) color, see below

## Material You

On devices supporting dynamic themes Kustom will try to extract the palette definition from the System. Following colors
will then be available as variables through the [System Info](/docs/reference/functions/si):

-   Primary system accent color: `$si(sysca1, XX)`
-   Secondary system accent color: `$si(sysca2, XX)`
-   Tertiary system accent color: `$si(sysca3, XX)`
-   Primary system neutral color: `$si(syscn1, XX)`
-   Secondary system neutral color: `$si(syscn2, XX)`

On all colors you can specify XX as the shade level, a lower number will give you a darker color, a higher number will
give you a lighter color.

### Resource names

Android uses an inverted shade convention for material you colors in the range 1000 to 0, so the darkest color is 1000
and the lightest is 0. Kustom instead goes from 0 (darkest) to 100 (white). To convert from Kustom to Android shade just 
invert and remove one zero, so:

-   system_accent1_1000 -> $si(sysca1, 0)
-   system_accent2_100 -> $si(sysca2, 90)
-   system_accent3_0 -> $si(sysca3, 100)
-   system_neutral1_10 -> $si(syscn1, 99)

### Compatibility

Devices below Android 12 or with no support for dynamic colors will still work with the functions above but Kustom will
fallback to the default wallpaper based palette and use HSV to generate the the correct shade.

### Known Issues

Some system (like Samsung) has its own dynamic color theming set and Samsung created alongside with Google dynamic theme
color set. Those values are not matching. Basically in OneUI 5, you have 2 sets of Color Theme. The whole system will
use the Samsung one but Kustom apps will get the values from the Google palette. A solution is being investigated.
