---
title: Global Variables
type: docs
categories:
  - Reference
tags:
  - functions
  - formula
  - globals
  - gv
---
# How to use Globals

If you open the Advanced Editor you will notice on the first "root" container a tab called "globals", that section will
allow you to add / remove and change Global variables which are just settings that can be applied to multiple modules at
once. So, how to use them?

So let's say you have a preset with a lot of text items around and you decide that you want to change the font of all of
these very quickly from a single setting point, so, in this case, you use a Global, so you:

* Create a Global of type "Font" in the Globals section mentioned above
* You go in the TextModule, select the "Font" preference and then click on the "Globe" icon in the actionbar on top
* Then you click again on the option and select the Global you just created from the list
* Repeat this for all the Text modules you want to change with that global

That's it! You can now change the font of all these modules just changing the Global preference in the main container.
