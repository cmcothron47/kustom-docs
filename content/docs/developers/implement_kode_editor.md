---
title: Implement a Kode Editor
type: docs
categories:
  - Developers
tags:
  - Developers
  - kode editor
---

# Implement a Kode Editor

Starting from 3.08 Kustom allows external apps to edit Kode from the formula editor fragment. A specific button will appear on top of the editor as soon as an app providing the right Action in the Manifest will be detected, this app just needs to return the new value of the formula that will then appear in the editor. Extras will be provided in the future to get current formula being edited and globals available to the user (in Json format). Right now only basic implementation is available.  
  
Implementing an app it's very easy, you can either check the sample one [here](https://bitbucket.org/frankmonza/kodeeditorsample) or follow these simple tasks:

### Add correct action in the Manifest

In your AndroidManifest.xml add the following action to the Editor Activity intent-filter section:

`<action android:name="org.kustom.intent.action.EDIT_KODE" />`

### Properly send Kode as a result

Your app will be started using "startActivityForResult()" call so you will have to properly set a result before finishing (so when Kode has been picked/edited). To do so create an Intent like this:

`String kode = "My edited Kode";  `
`i.putExtra("org.kustom.KODE", kode);  `
`// Weather to append or not result in the calling editor  `
`i.putExtra("org.kustom.extra.APPEND", v.getId() == R.id.kode_append);  `
`setResult(RESULT_OK, i);`

### Optional: data from source intent

Right now only current Kode in the editor is available as extra and called "org.kustom.extra.KODE", you can easily fetch it from your activity by doing:

`if (getIntent() != null) { ` 
`    String text = getIntent().getStringExtra("org.kustom.extra.KODE");  `
`// Do something with text  `
`}`

### Optional: provide examples in the functions samples list

Starting from 3.15 Kustom allows external plugins to show their own list of examples in the functions list, in order to do so you need to implement a content provider, best way is to copy from the [Sample Kode Editor](https://bitbucket.org/frankmonza/kodeeditorsample) and do the following:

* Copy the provider lines from the sample [AndroidManifest.xml](https://bitbucket.org/frankmonza/kodeeditorsample/src/550fb067d1b940126a2702ce12a7d7a62cb00b39/app/src/main/AndroidManifest.xml?at=master&fileviewer=file-view-default)
* Change the android:authorities to whatever you like, use pkg naming convention
* Copy the [SampleProvider](https://bitbucket.org/frankmonza/kodeeditorsample/src/550fb067d1b940126a2702ce12a7d7a62cb00b39/app/src/main/java/org/kustom/kodersample/SampleProvider.java?at=master&fileviewer=file-view-default) file in your code, if you change pkg or name please also remember to align the AndroidManifest.xml file
* Implement your login in the "query" method, the current one is already providing 2 examples