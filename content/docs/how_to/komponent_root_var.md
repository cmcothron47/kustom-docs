---
title: How to link komponent and root global variables 
type: docs
categories:
  - How To
tags:
  - How To
  - global variables
  - komponent
---

# How to link komponent and root global variables

There are instances when you need to access your root global variables from a komponent. To do this, the two variables needs to be linked. The steps below demonstrate how this is done.

1. Create a Text global variable (`rootVar`) in your root directory with some data

<img src="https://imagizer.imageshack.com/img922/1893/ad1Gjb.jpg" alt="rootVar" width="300">

2. Create another Text global variable (`kVar`) in your komponent and leave it empty

<img src="https://imagizer.imageshack.com/img924/2936/rRWkPM.jpg" alt="rootVar" width="300">

3. Tap on the check box next to your list `kVar`
4. Tap on the "globe" icon from the top right

<img src="https://imagizer.imageshack.com/img924/8115/dGkCsV.jpg" alt="rootVar" width="300">

5. Tap on the "globe" icon that's appended to your `kVar` variable

<img src="https://imagizer.imageshack.com/img922/1152/9d989f.jpg" alt="rootVar" width="300">

6. Select `rootVar` from the list

<img src="https://imagizer.imageshack.com/img924/5226/WoeLgB.jpg" alt="rootVar" width="300">

7. Add a Text element in your komponent with the following formula: `$gv(kVar)$`

<img src="https://imagizer.imageshack.com/img923/5471/EJcPdk.jpg" alt="rootVar" width="300"> <img src="https://imagizer.imageshack.com/img922/7565/qZg5k4.jpg" alt="rootVar" width="300">

** expected result: the value of `rootVar` will be displayed when `kVar` is called