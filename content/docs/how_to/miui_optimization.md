---
title: How to turn off MIUI Optimization in Xiaomi devices 
type: docs
categories:
  - How To
tags:
  - How To
  - MIUI Optimization
  - xiaomi
---

# How to turn off MIUI Optimization in Xiaomi devices

MIUI Optimization is meant to help boost Xiaomi phone’s performance but sometimes it can cause a variety of issues on non-MIUI-based apps such us the Kustom apps. To turn it off, you can use the steps below:

{{< col >}}

### Steps

1. Head to your device's settings
2. Scroll down and look for "Additional settings"
3. Now look to find Developer Options. If it’s not visible, go to the about section on the settings and tap on the MIUI version, keep tapping until it displays “You are a developer now”. Once you get this message, head over to the Advanced settings and you’ll find the developer option.
4. Scroll down and look for "MIUI Optimization" and turn it off.

- If you're missing the MIUI Optimization option on MIUI 13 or above, go to Developer options and use the "Reset to default values" button at the bottom of the page. You may also check if your device's refresh rate is currently set to 120Hz. If yes, set that to 60Hz before doing the reset and check again.

- If you're missing the Developer options, go to your device's settings > About phone > Tap on the MIUI version multiple times

<--->

### Video

<img src="https://imagizer.imageshack.com/img923/379/nEgT9v.gif" alt="MIU Optimization" width="300">

<--->

{{< /col >}}



