---
title: How to use complications to launch an app in the watch with KWCH
type: docs
categories:
  - How To
tags:
  - How To
  - KWCH
  - Complications
---

# How to use complications to launch an app in the watch with KWCH

1. Create an object in the KWCH editor to trigger the touch function from

<img src="https://imagizer.imageshack.com/img924/3267/G1b7Um.jpg" alt="rootVar" width="300">

2. Add a Touch function to it
3. Set action to "Launch complication activity"
4. Set a complication number between 1 to 4
5. Push the watch face to the watch by tapping on the green watch button at the top

<img src="https://imagizer.imageshack.com/img922/5469/OqM60u.jpg" alt="rootVar" width="300">

6. On the watch, tap and hold on the KWCH watch face

<img src="https://imagizer.imageshack.com/img923/7948/PY5mp2.png" alt="rootVar" width="300">

7. Select "Customize"

<img src="https://imagizer.imageshack.com/img922/2299/gzt4jd.png" alt="rootVar" width="300">

8. Tap on "Data"

<img src="https://imagizer.imageshack.com/img923/3114/H66TS6.png" alt="rootVar" width="300">

9. Select the slot that corresponds to the complication number in step 4

<img src="https://imagizer.imageshack.com/img924/1610/bDLrg1.png" alt="rootVar" width="300">

10. Select "App shorcut"

<img src="https://imagizer.imageshack.com/img923/5995/bcL6SC.png" alt="rootVar" width="300">

11. Select the watch app you want to launch

<img src="https://imagizer.imageshack.com/img922/2083/3YIzEB.png" alt="rootVar" width="300">



*Note: The steps on the watch side may vary depending on the the make and model of the smart watch