---
title: How to send variables from Tasker
type: docs
categories:
  - How To
tags:
  - How To
  - Tasker
  - broadcast receiver
---

# How to send variables from Tasker

With Kustom's Tasker integration, you should now be able to send your Tasker variables to the Kustom apps using the Broadcast receiver function and a tasker plugin. To do this:

### Sending a variable from a Tasker task

1. Open Tasker and go to the Tasks tab
2. Add a new task by tapping on the "+" button from the bottom right

<img src="https://imagizer.imageshack.com/img924/2154/wHD7ls.jpg" alt="Tasker Task" width="300">

3. Give your task a name and tap on the "check" button

<img src="https://imagizer.imageshack.com/img923/3430/lJglGF.jpg" alt="Tasker Task" width="300">

4. Tap the "+" button to add a new action

<img src="https://imagizer.imageshack.com/img922/5615/FG8WFs.jpg" alt="Tasker Task" width="300">

5. Go to the Plugin category

<img src="https://imagizer.imageshack.com/img922/5435/qpqkU0.jpg" alt="Tasker Task" width="300">

6. Select a Kustom app on the list
7. Select the Send Variable kustom action

<img src="https://imagizer.imageshack.com/img924/2020/RIxAeq.jpg" alt="Tasker Task" width="300">

8. Tap on the "pen" icon next to Configuration

<img src="https://imagizer.imageshack.com/img923/4182/XWscra.jpg" alt="Tasker Task" width="300">

9. Provide the Kustom Variable and Tasker String and tap on the "check" button

<img src="https://imagizer.imageshack.com/img924/8692/zLU1O3.jpg" alt="Tasker Task" width="300">

10. Tap on the back arrow

<img src="https://imagizer.imageshack.com/img922/9434/r4cYT8.jpg" alt="Tasker Task" width="300">

11. Tap on the "Play" button to execute the action

<img src="https://imagizer.imageshack.com/img924/8104/fJQKjx.jpg" alt="Tasker Task" width="300">

### Receiving the Tasker variable from the Kustom app

1. Add a text element to your preset
2. Change the Text attribute formula to `$br(Tasker, kVar)$`, where kVar is the Kustom Variable name you set from step 9 of the previous part.

<img src="https://imagizer.imageshack.com/img922/1492/noXJzQ.jpg" alt="Tasker Task" width="300">

3. Save and check your widget

<img src="https://imagizer.imageshack.com/img923/9776/ImKzBg.jpg" alt="Tasker Task" width="300">

** You can apply the Tasker task you just created to any of your Tasker profile