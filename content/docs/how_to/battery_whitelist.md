---
title: How to whitelist Kustom from Battery Optimization
type: docs
categories:
  - General Information
tags:
  - General Information
  - battery
  - whitelist
---
# How to whitelist Kustom from Battery Optimization

Android is very aggressive with background services and Kustom unfortunately relies on them for a lot of functionalities like updating the clock, keeping statistics around battery and connectivity usage, interacting with tasker and much more. This means that it requires to be always running to work properly, in order to do so it either needs to display an always visible notification or it has to be removed from Android battery optimization. To remove Kustom you need to do the following:

* **Android 6.0+** the location of the setting may vary per device, but a common method is to launcher the "Android Settings" app, select "Apps", click the cog icon on the top-right, then "Battery Optimization". Click "All Apps", then Kustom, then "Do Not Optimize". You can find a step by step guide at [this URL](https://www.techrepublic.com/article/how-to-remove-android-apps-from-the-battery-optimization-list/)
* **Miui** launch the "Settings" app, select "Additional Settings" then "Battery & Performance" finally set "Manage Apps Battery Usage" to OFF, more info [HERE](https://dontkillmyapp.com/xiaomi)
* **Samsung** some devices have an app called "Smart Manager", launch the app, press the battery quarter of the screen, under "App Optimization" press "Detail" where you can adjust the settings for each app, then disable Kustom, more info [HERE](https://dontkillmyapp.com/samsung)
* Any other vendor check [https://dontkillmyapp.com](https://dontkillmyapp.com)