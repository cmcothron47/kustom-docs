---
title: How to display data using complications with KWCH
type: docs
categories:
  - How To
tags:
  - How To
  - KWCH
  - Complications
---

# How to display data using complications with KWCH

1. Add a text element in the KWCH editor

<img src="https://imagizer.imageshack.com/img924/3677/QGP3KW.jpg" alt="rootVar" width="300">

2. Add a Complication Data formula to it

<img src="https://imagizer.imageshack.com/img924/3895/GVvIdp.jpg" alt="rootVar" width="300">

3. Push the watch face to the watch by tapping on the green watch button at the top

<img src="https://imagizer.imageshack.com/img924/3083/HC08Z2.png" alt="rootVar" width="300">

4. On the watch, tap and hold on the KWCH watch face
5. Select "Customize"

<img src="https://imagizer.imageshack.com/img922/9050/HjtLCE.png" alt="rootVar" width="300">

6. Tap on "Data"

<img src="https://imagizer.imageshack.com/img923/3114/H66TS6.png" alt="rootVar" width="300">

7. Select the slot that corresponds to the complication number in step 4

<img src="https://imagizer.imageshack.com/img924/1610/bDLrg1.png" alt="rootVar" width="300">

8. Select a data that you want displayed

<img src="https://imagizer.imageshack.com/img924/4509/IxSYSG.png" alt="rootVar" width="300">

<img src="https://imagizer.imageshack.com/img924/3083/HC08Z2.png" alt="rootVar" width="300">


*Note: The steps on the watch side may vary depending on the the make and model of the smart watch