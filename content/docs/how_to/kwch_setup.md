---
title: How to setup KWCH
type: docs
categories:
  - How To
tags:
  - How To
  - KWCH
  - Complications
---

# How to setup KWCH

1. Install [KWCH Kustom Watchface Creator](https://play.google.com/store/apps/details?id=org.kustom.watchface&hl=en_US) on your phone.

2. On your watch running Android wear, open the Google Play Store

<img src="https://imagizer.imageshack.com/img923/2773/cbHcOf.png" alt="rootVar" width="300">

3. Search for "KWCH"

<img src="https://imagizer.imageshack.com/img924/1140/wxGRWu.png" alt="rootVar" width="300">

4. Install the app (Can also be installed from the the watch manager app on your phone)

<img src="https://imagizer.imageshack.com/img924/4329/irDOiX.png" alt="rootVar" width="300">

5. Go back to your phone and open KWCH
6. Load/build your preset
7. Once done, tap on the green watch icon at the top to push the watch face to your smart watch

<img src="https://imagizer.imageshack.com/img922/1358/0nEB7f.jpg" alt="rootVar" width="300">

8. On your smart watch, tap and hold on your current watch face

<img src="https://imagizer.imageshack.com/img923/9356/WWhb0c.png" alt="rootVar" width="300">

9. Change it to KWCH

<img src="https://imagizer.imageshack.com/img922/493/TbnNfx.png" alt="rootVar" width="300">
