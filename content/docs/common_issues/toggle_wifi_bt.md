---
title: Toggle WiFi and Bluetooth not working
type: docs
categories:
  - Common Issues
tags:
  - Common Issues
  - Bluetooth
  - Wifi
---
# Toggle WiFi and Bluetooth not working
Every Android app needs to target a specific API version, Kustom up to 3.48 was targeting API release 28. Unfortunately starting from November 2020 apps on the Google Play require developers to update their target API level to 29 to make a new app release. This is usually not an issue but API 29 introduces a new restriction due to security reasons that prevents developers to switch WiFi or BT programmatically. For this reason starting from Kustom v3.49 that capability was removed.

As a reference original issue reported to Google is the following:

[https://issuetracker.google.com/issues/128554616](https://issuetracker.google.com/issues/128554616)

The answer from Google is to show instead the connectivity Dialog available from Android 11 but for Kustom this doesnt make a lot of sense as its available on Android 11 only so for now capability has been removed.
