---
title: Getting a glitch or a noise when rendering KLWP
type: docs
categories:
  - Common Issues
tags:
  - Common Issues
  - KLWP
  - glitch
  - noise
---

# Getting a glitch or a noise when rendering KLWP

When applying a theme on some devices, you will end up getting a distorted live wallpaper. You will either get distorted elements or some sort of "noise" on the screen (see screenshots below for reference). This has something to do with rendering. To get around this, turn on the "Disable Parallel Rendering" setting in the app using the steps below.

1. Open KLWP
2. Go to App settings
3. Advance options
4. Turn on "Disable Parallel Rendering"

Screenshots:

<img src="https://imagizer.imageshack.com/img924/4280/9xsRmb.jpg" alt="noise" width="300px"> <img src="https://imagizer.imageshack.com/img922/3131/csmYBq.png" alt="glitch" width="300px">