---
title: KWGT widget not updating or delayed
type: docs
categories:
  - Common Issues
tags:
  - Common Issues
  - widget
  - kwgt
  - stuck
---

# KWGT widget not updating or delayed

There are times when your KWGT widget seems to be stuck. The data on it, like time or weather, is not changing or delayed. When this happens, you can check on the following.

First, make sure that KWGT is allowed to run in the background as a service and is not being blocked by some optimization tools. This includes your device's own battery optimization functionality. Other 3rd party apps like Greenify, Purify, or Samsung's Smart Management can also cause this kind of issue.

Second, you can force the app to update more frequently. KWGT can technically refresh every second. This, however, have some drawbacks due to some general Android Widget limitations. Android widgets can't tell if they are being viewed or not and will continue to run and will attempt to render even if you are using a different app. If you still want to do this, simply go to KWGT's app settings >> Advanced options >> Change "Update mode" to "Fast".