---
title: Kustom app is showing the wrong Next Alarm
type: docs
categories:
  - Common Issues
tags:
  - Common Issues
  - alarm
---

# Kustom app is showing the wrong Next Alarm

When using `$si(alarmt)$` or any of the alarm related formulas, the Kustom app will display some semingly random values which you can't find in your Alarm app. There are a couple of reasons why this happens.

First, you might be using a 3rd party alarm or clock app that's not supported. Always reference to the default Clock/Alarm app of your device as most of them are compliant and therefore compatible.

Second, if you're using Tasker, some alarm mechanism within Tasker will interfere with the broadcast. To get around this, go to your Tasker Preference >> Monitor tab >> set "Use Reliable Alarms" to "Never".

3rd, on some devices, like Samsung, the calendar app will broadcast reminders as  alarms which the Kustom app will detect and display. Make sure to check your Samsung calendar app if the value that you're getting are coming from a calendar reminder.