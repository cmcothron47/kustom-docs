---
title: Disable / Lock Touch in KWGT Widgets
type: docs
categories:
  - Common Issues
tags:
  - Common Issues
  - Touch
  - Disable
---
# Disable / Lock Touch in KWGT Widgets

By default a lot of Widgets will open the Editor on touch, this is just because by default the touch action associated with the entire widget is to open the editor, to change that behaviour just do the following:

* Open KWGT editor
* Go to the "touch" tab
* Remove or set to disabled the touch action associated with opening the editor

In general you can change ANY touch action going to the editor int he root and selecting the "Shorcuts" tab where you find all the events triggered by the root or any other inner module.