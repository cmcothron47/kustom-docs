---
title: Application Blocked error when trying to link Google Fit in KWGT
type: docs
categories:
  - Common Issues
tags:
  - Common Issues
  - google fit
  - kwgt
  - blocked
---

# "Application Blocked" error when trying to link  Google Fit in KWGT

1. Install [KLWP](https://play.google.com/store/apps/details?id=org.kustom.wallpaper&hl=en&gl=US) from the Play Store
2. Open KLWP 
3. Create a test theme
4. Insert a Text element with `$fd(steps)$`
5. Save and link your Google Fit account this way
6. If successful, go back to KWGT and check if this gets linked with your Google Fit account as well

