---
title: Animated GIF Support
type: docs
categories:
  - Common Issues
tags:
  - Common Issues
  - Touch
  - Disable
---

# Animated GIF Support

KLWP and KLCK do support animated GIFs but this option has been hidden due to high battery usage of this feature, the capability will be enabled again as soon as i will find a way to do this efficiently without impacting battery. This said you can still enable GIFs in Kustom by doing the following:

* Select the following code and copy it to your Android device clipboard:
    
        ##KUSTOMCLIP## { "clip_version": 1, "clip_cut": [], "clip_modules": [ { "internal_type": "MovieModule" } ] } ##KUSTOMCLIP##
    
* Open Kustom go to the editor in the ROOT folder and in the LAYERS editor, then press PASTE, a new Movie Module will be added to your design
* You can now add an Animated GIF, you can then also copy / paste this item between your designs