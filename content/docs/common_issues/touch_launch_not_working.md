---
title: Touch or Launch App Not Working 
type: docs
categories:
  - Common Issues
tags:
  - Common Issues
  - Touch
  - Launch app
---

# Touch or Launch App Not Working

On some device especially **XIAOMI** or **ONEPLUS** touch or launch app might not be working, to fix this please do the following:

* Ensure Kustom and the Launcher are removed [from battery optimization](https://docs.kustom.rocks/docs/how_to/battery_whitelist/)
* Open Kustom settings, on the top select **Notification Mode** and then press **FORCED**, you can still hide the notification from android settings please check [this article](https://docs.kustom.rocks/docs/general_information/persistent_notifciation/)
* \*Optional\* grant Kustom Notification access: from **Android settings** click **Apps & Notifications** -\> **Special App Access** -\> **Notifications** and grant access to Kustom app

You can also check [https://dontkillmyapp.com](https://dontkillmyapp.com) for more info depending on your device vendor

**XIAOMI**

* Also ensure this permission is set to yes: Apps > Permissions > Other permissions > KLWP > Settings > Home screen shortcuts **SET TO YES**

**POCO**

* Enable "Display pop-up windows while running in the background" on Apps -> KLWP