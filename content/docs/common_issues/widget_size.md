---
title: Widget keeps resizing by itself
type: docs
categories:
  - Common Issues
tags:
  - widget
  - size
  - KWGT
---

# Widget keeps resizing by itself

On some devices, KWGT widgets will automatically resize by themselves. This can be triggered in different ways. Mostly, when the device is being restarted. The issue is commonly encountered on devices with multiple screen dimensions like foldable devices. Here are a couple of things that you can do to try to get around the issue:

- Please ensure that widget sizing is not set to "Auto" in app settings.
- Check that your device rotation is detected correctly in the settings it should show something like "Default (Landscape)" if it's not your correct location force the correct one.
- Always resize the widget before loading it, so add the widget and then long press on it and drag the borders to bring it to its final size, then finally click on it to load the preset.
- If not already scaled correctly adjust widget size using "scale" option in the layer tab.
- On devices with multiple screen dimensions, you can detect the screen dimension using the "si" formula, like `$si (swidth)$` or `$si(sheight)$`, and have our widget respond accordingly