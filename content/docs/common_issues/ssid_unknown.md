---
title: SSID Shows UNKNOWN
type: docs
categories:
  - Common Issues
tags:
  - Common Issues
  - SSID
  - Wifi
---
# SSID Shows UNKNOWN

Android 8.1 or greater requires Location access to show the SSID, if an app has not that permission granted then it will not be able to access the WiFi network name. So if you see "unknown" displayed as the wireless SSID name then please

* Ensure that Kustom has Location access, long press on Kustom app icon from the launcher, select "info" then "permissions" and grant Location to it. 
* Ensure you did not disable Location services (check the status bar options)
* If you have Android 10 or later ensure you gave Kustom access to Location ALL THE TIME and not ONLY WHEN RUNNING, this is also required to display the SSID