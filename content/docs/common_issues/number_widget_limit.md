---
title: Unable to add more than a certain number of KWGT widgets
type: docs
categories:
  - Common Issues
tags:
  - Common Issues
  - widget limit
  - kwgt
---

# Unable to add more than a certain number of KWGT widgets

Unfortunately some devices from China, like Oppo, added a non sense limitation to its launcher. There is a limit on the number of similar widgets you can add to your homescreen. Some you can only add one and for some you can only add up to five depending on the device. This is something that we cannot fix from end. It's really annoying because it is definitely not the standard Android behavior. The only solution is to use an alternative launcher such as [Smart Launcher](https://play.google.com/store/apps/details?id=ginlemon.flowerfree&hl=en&gl=US) or [Nova Launcher](https://play.google.com/store/apps/details?id=com.teslacoilsw.launcher&hl=en&gl=US).