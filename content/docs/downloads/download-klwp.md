---
title: KLWP
type: docs
categories:
  - Downloads
tags:
  - klwp
  - apk
  - aosp
  - changelog
---
<!-- <img src="https://appcenter-filemanagement-distrib5ede6f06e.azureedge.net/24064e8a-a80e-4ac1-a839-12d4e54a8590/9w.png?sv=2019-07-07&sr=c&sig=dRhKAyhRPzGHC06UEuM%2FsAGKupeo3Wlkw3WYghyi7Ic%3D&se=2024-06-27T14%3A16%3A24Z&sp=r" width="50" /> -->
# KLWP downloads


## Latest stable: 3.75
**Download**: <b><a href="https://kustom.rocks/download/KLWP/965">Google Play APK</a></b><br/>
Build: 3.75b410013<br/>
Size: 31.53mb<br/>
Uploaded: 2024-04-09T14:30:53.207Z<br/>







## Latest beta: 3.76
**Download**: <b><a href="https://kustom.rocks/download/KLWP/967">Google Play APK</a></b><br/>
Build: 3.76b417112beta<br/>
Size: 31.29mb<br/>
Uploaded: 2024-06-19T13:32:16.633Z<br/>


### Variants
  - **AOSP**: <a href="https://kustom.rocks/download/KLWP/968">3.76b417214aosp</a> (No PRO required, ADS supported, no Google Services)
  - **Huawei**: <a href="https://kustom.rocks/download/KLWP/966">3.76b416913huawei</a> (App Gallery APK)
 



### Previous builds


## Previous releases:

### v3.74<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/951">Google Play APK</a></b><br/>
Build: 3.74b331712<br/>
Size: 29.32mb<br/>
Uploaded: 2023-11-13T12:45:16.412Z<br/>
Release Notes:
None

### v3.73<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/929">Google Play APK</a></b><br/>
Build: 3.73b314511<br/>
Size: 28.85mb<br/>
Uploaded: 2023-05-25T11:06:06.408Z<br/>
Release Notes:
<ul style="margin: 0"><li>Added new free path in Shapes, draw ANYTHING you want! Check out https://kustom.rocks/svgpath</li><li>Added confirm dialog on preset delete</li><li>You can now spell numbers and clock in Japanese</li><li>You can now parse dates in ISO or other formats via dp()</li><li>Hiding duplicates in font picker</li><li>Fixed issues with preset import on file open</li><li>Fixed sizing issues on foldable screens</li><li>Fixed flow issue not hiding when closing bottom menu</li><li>Fixed gradients in squircle shape</li><li>Fix KLWP disappearing objects when close to the edge</li><li>Fix issue on Samsung devices when adding a shortcut</li><li>Fix Kustom forgets Tasker vars not updated for more than 6 days</li></ul>

### v3.72<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/925">Google Play APK</a></b><br/>
Build: 3.72b311310<br/>
Size: 29.39mb<br/>
Uploaded: 2023-04-23T10:17:01.532Z<br/>
Release Notes:
<ul style="margin: 0"><li>You can now set globals using an URI like kwgt://global/name/value</li><li>Fixed new presets not shown after export or import</li><li>Fixes lag on KLWP in some preset</li></ul>

### v3.71<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/918">Google Play APK</a></b><br/>
Build: 3.71b308214<br/>
Size: 29.37mb<br/>
Uploaded: 2023-03-23T15:20:06.570Z<br/>
Release Notes:
<ul style="margin: 0"><li>You can now C style use /* comments */ inside $kustom expressions$</li><li>Brand new export dialog, feedback welcome!</li><li>You can now export as image</li><li>Flows can open URI and Intent links (so you can set Tasker vars)</li><li>Flows can store any type of globals not just text globals</li><li>Fixes new preset being re-exported causing duplicates</li><li>Fixes music player info providing wrong package name</li><li>Fixes feels like temperature not using provider data in AccuWeather</li><li>Fixed "$wi(tempu)$" not changing when settings changed</li></ul>

### v3.70<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/907">Google Play APK</a></b><br/>
Build: 3.70b303210<br/>
Size: 27.11mb<br/>
Uploaded: 2023-02-01T15:32:57.948Z<br/>
Release Notes:
<ul style="margin: 0"><li>Added Kustom Flows to bring you where no one has gone before https://kustom.rocks/flows</li><li>Added Secret Globals to store sensitive data like API keys in locked komponents</li><li>Added support for remote messages (http post from the internet), see https://kustom.rocks/remotemsg</li><li>Added support for Material You "Monet" accent and neutral colors via $si(sysca1/a2/a3/n1/n2, shadelevel)$ function https://kustom.rocks/dyncolors</li><li>KLWP now detects if its zoomed via $si(wgzoomed)$, can be used on some launcher to understand when drawer or recents are opened</li><li>Improved speed of listing entries / fonts from SD</li><li>Fixed KWGT not correctly updating when a global was changed</li><li>Fixed YRNO weather provider current conditions not always displayed</li></ul>

### v3.63<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/863">Google Play APK</a></b><br/>
Build: 3.63b228708<br/>
Size: 24.22mb<br/>
Uploaded: 2022-10-14T09:49:44.318Z<br/>
Release Notes:
<ul style="margin: 0"><li>New lv() function can set local variables</li><li>In KWGT si(darkmode) now only returns system dark mode</li><li>Added si(darkwp) that returns 1 when wallpaper colors prefer a dark theme</li><li>Formula faves are now also stored on external storage as a backup measure</li></ul>

### v3.62<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/851">Google Play APK</a></b><br/>
Build: 3.62b224415<br/>
Size: 23.82mb<br/>
Uploaded: 2022-09-01T17:01:40.099Z<br/>
Release Notes:
<ul style="margin: 0"><li>Font picker now shows current text as preview</li><li>Font picker UI improvements</li><li>Fixed duplicate issue on exporting some preset</li><li>Fixes notifications export issues</li><li>Fixes random image and file not working even in Kustom subfolders</li></ul>

### v3.61<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/842">Google Play APK</a></b><br/>
Build: 3.61b223012<br/>
Size: 23.81mb<br/>
Uploaded: 2022-08-18T14:48:01.266Z<br/>
Release Notes:
<ul style="margin: 0"><li>Improved save speed for presets with lots of resources</li><li>Slightly improved external font / font packs loading speed</li><li>Fixed KLWP 5 secs delay</li><li>Fixed font picker not remembering scroll position</li><li>Fixed alpha ordering in font picker</li><li>Fixed recent/faves not working for local storage presets</li></ul>

### v3.60<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/821">Google Play APK</a></b><br/>
Build: 3.60b220710<br/>
Size: 23.79mb<br/>
Uploaded: 2022-07-26T11:53:10.530Z<br/>
Release Notes:
<ul style="margin: 0"><li>Fixed local fonts not saved</li></ul>

### v3.59<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/818">Google Play APK</a></b><br/>
Build: 3.59b220615<br/>
Size: 23.79mb<br/>
Uploaded: 2022-07-25T16:49:38.021Z<br/>
Release Notes:
<ul style="margin: 0"><li>Hot fix exporting presets will not store resources</li></ul>

### v3.58<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/806">Google Play APK</a></b><br/>
Build: 3.58b217313<br/>
Size: 23.79mb<br/>
Uploaded: 2022-06-22T14:57:01.515Z<br/>
Release Notes:
<ul style="margin: 0"><li>Removed access to SD Card due to new security policies, storage migration will be requested</li><li>New font picker now shows Google fonts automatically</li><li>KLWP workaround for Samsung Android 12 bug</li><li>KLWP ignores wallpaper color for dark mode</li><li>You can purchase KWGT without separate pro key</li><li>A big thanks to all the users keeping translations up to date! You rock!</li></ul>

### v3.57<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/759">Google Play APK</a></b><br/>
Build: 3.57b121813<br/>
Size: 20.57mb<br/>
Uploaded: 2021-08-06T14:54:02.961Z<br/>
Release Notes:
<ul style="margin: 0"><li>Packs are now correctly showing newer items first in loader</li><li>Added Projekt launcher support on KLWP</li><li>Removed Yahoo weather :(</li></ul>

### v3.56<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/753">Google Play APK</a></b><br/>
Build: 3.56b114416<br/>
Size: 20.47mb<br/>
Uploaded: 2021-05-24T16:57:20.776Z<br/>
Release Notes:
<ul style="margin: 0"><li>Fixes color picker not showing recent colors</li><li>Fixes load preset tasker action</li><li>Fixes load preset from file explorer / telegram</li><li>Fixes color picker unusable on small screens</li></ul>

### v3.55<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/747">Google Play APK</a></b><br/>
Build: 3.55b112308<br/>
Size: 20.52mb<br/>
Uploaded: 2021-05-03T09:42:01.892Z<br/>
Release Notes:
<ul style="margin: 0"><li>New color picker, feedback welcome!</li><li>Added support for global folders</li><li>Fix issues with TF and settings on some device</li><li>Fix empty filter locks you in the loader</li></ul>

### v3.54<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/740">Google Play APK</a></b><br/>
Build: 3.54b106810<br/>
Size: 20.16mb<br/>
Uploaded: 2021-03-09T11:24:01.520Z<br/>
Release Notes:
<ul style="margin: 0"><li>Fix loading directly from packs dashboard not working</li><li>Fix issues with location search</li><li>Language updates</li></ul>

### v3.53<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/735">Google Play APK</a></b><br/>
Build: 3.53b104015<br/>
Size: 19.93mb<br/>
Uploaded: 2021-02-09T16:17:15.261Z<br/>
Release Notes:
<ul style="margin: 0"><li>New purchase dialog, feedback welcome</li><li>You can now browse installed packs in the main loader</li><li>Fix YrNo will stop working 31st of March</li><li>Fix KLWP sometimes launching wrong app</li><li>Fix pressing edit in the widget will then make load preset fail</li><li>Fix issues on Samsung when in Airplane Mode</li><li>Fix preview scaling in KLWP loader</li><li>Fix hardcoded max at 720 in number globals</li></ul>

### v3.52<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/722">Google Play APK</a></b><br/>
Build: 3.52b102617<br/>
Size: 19.91mb<br/>
Uploaded: 2021-01-26T19:59:27.567Z<br/>
Release Notes:
<ul style="margin: 0"><li>New loader window, feedback welcome!</li><li>Added tc(url) to encode URLs</li><li>Added si(powersave) to show when power save is on</li><li>Fixed traditional and simplified Chinese in language settings</li><li>Fixed delay to launch in Android 11 for KLWP</li><li>Fixed location search not working in some language</li><li>Fixed crop not working in the editor</li><li>Fixed BT state not updated on change</li><li>Fixed SIM count on some device</li></ul>

### v3.51<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/700">Google Play APK</a></b><br/>
Build: 3.51b30911<br/>
Size: 19.64mb<br/>
Uploaded: 2020-11-04T11:43:55.950Z<br/>
Release Notes:
<ul style="margin: 0"><li>New intro and settings</li><li>New KLWP control refresh rate in the advanced settings</li><li>Fixes Chinese language cannot be forced</li><li>Fixed some music players not being recognized</li><li>Fixes slow downs / battery issues when using palette formulas</li><li>Fixes battery duration / last plugged issues on Android 11</li></ul>

### v3.50<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/678">Google Play APK</a></b><br/>
Build: 3.50b28512<br/>
Size: 14.34mb<br/>
Uploaded: 2020-10-11T12:56:16.780Z<br/>
Release Notes:
<ul style="margin: 0"><li>HOTFIX fonts and icon fonts picker not working</li></ul>

### v3.49<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/677">Google Play APK</a></b><br/>
Build: 3.49b27509<br/>
Size: 14.34mb<br/>
Uploaded: 2020-10-01T09:35:48.771Z<br/>
Release Notes:
<ul style="margin: 0"><li>Added support for battery level of BT devices (Android 8 or newer)</li><li>Added support for 5G network type</li><li>Added squircle shape so you can get that IOS picture widget :)</li><li>Slightly faster export / import</li><li>Removed toggle WiFi and BT due to API 29 upgrade, sorry, blame Google</li><li>KLWP has a new option to handle raw touch on launchers where touch does not work</li><li>You can copy and paste globals</li><li>Fixed TS unit wrong</li><li>Fixed TU not working with negative values</li><li>KLCK Fixed support for Android 10</li><li>KLCK Fixed lock not displaying on Android 8 or newer without system lock</li><li>KLCK Fixed music visualizer</li></ul>

### v3.48<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/660">Google Play APK</a></b><br/>
Build: 3.48b21017<br/>
Size: 14.12mb<br/>
Uploaded: 2020-07-28T17:21:21.600Z<br/>
Release Notes:
<ul style="margin: 0"><li>KLWP Added support for music visualization</li><li>KLWP General availability of Movie Module</li><li>Added rounded corners to triangles, exagons</li><li>Added CJK for Chinese chars in tc(type)</li><li>Weather plugin now supports also WeatherBit and, for Australia, WillyWeather</li><li>Fixed force weather update touch action not working</li><li>Fixed download/upload speed showing wrong values realtime</li><li>Fixed animation formulas not showing formula value</li></ul>

### v3.47<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/648">Google Play APK</a></b><br/>
Build: 3.47b16716<br/>
Size: 14.33mb<br/>
Uploaded: 2020-06-15T16:31:30.321Z<br/>
Release Notes:
<ul style="margin: 0"><li>New you can select multiple preferred music players in the settings</li><li>Increased max loops in for loops</li><li>Fix preferred music player not forced when other players detected</li><li>Fix YT Music and Google Podcasts not being detected as players</li></ul>

### v3.46<br/>
Download: <b><a href="https://kustom.rocks/download/KLWP/644">Google Play APK</a></b><br/>
Build: 3.46b14609<br/>
Size: 14.14mb<br/>
Uploaded: 2020-05-25T09:28:52.311Z<br/>
Release Notes:
<ul style="margin: 0"><li>New scale modes in Bitmap Module (fit height, center fit and center crop)</li><li>New text module fit box mode to control height and width</li><li>New hex and decimal conversion in mu()</li><li>New count text occurences with tc(count)</li><li>Fix palette never returning black as a color</li><li>Fix in current not working on some device</li><li>Fix ce(contrast) returning black too often</li><li>Fix forcing Chinese language not working</li><li>KLWP Fix visibility issues during animations</li></ul>
