---
title: KUnread
type: docs
categories:
  - Downloads
tags:
  - kunread
  - apk
  - aosp
  - changelog
---
<!-- <img src="https://appcenter-filemanagement-distrib2ede6f06e.azureedge.net/1a0729d2-06d9-4a22-beb5-59d8649453b3/a05fa97002a5e6975f1718e648470bd1.png?sv=2019-07-07&sr=c&sig=YiKl%2FLtOOFB5kZa7VUafSh0dsMEHw8hyxKc7rsloOsQ%3D&se=2024-06-24T04%3A51%3A32Z&sp=r" width="50" /> -->
# KUnread downloads


## Latest stable: 1.0
**Download**: <b><a href="https://kustom.rocks/download/KUnread/1">Google Play APK</a></b><br/>
Build: 1.0<br/>
Size: 1.30mb<br/>
Uploaded: 2016-07-19T15:22:22.000Z<br/>







## Latest beta: 1.1
**Download**: <b><a href="https://kustom.rocks/download/KUnread/3">Google Play APK</a></b><br/>
Build: 1.1<br/>
Size: 1.68mb<br/>
Uploaded: 2018-12-03T15:23:13.000Z<br/>


### Release Notes
<ul style="margin: 0"><li>New icon</li><li>Plugin now supports Android 6 also (more info HERE)</li><li>Now targetting API 27</li></ul>

### Previous builds


## Previous releases:
