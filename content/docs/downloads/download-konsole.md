---
title: Konsole
type: docs
categories:
  - Downloads
tags:
  - konsole
  - apk
  - aosp
  - changelog
---
<!-- <img src="https://appcenter-filemanagement-distrib1ede6f06e.azureedge.net/a260ccc0-fe98-4656-a9d6-9e853fa9d664/ic_launcher.png?sv=2019-07-07&sr=c&sig=Jmn1uAUgL2rsvRsKRBDVM4AqXXgYTzhPXqIYFo9VG5A%3D&se=2024-06-21T21%3A11%3A30Z&sp=r" width="50" /> -->
# Konsole downloads


## Latest stable: 1.31
**Download**: <b><a href="https://kustom.rocks/download/Konsole/25">Google Play APK</a></b><br/>
Build: 1.31<br/>
Size: 19.82mb<br/>
Uploaded: 2024-04-04T14:09:18.332Z<br/>






## No beta right now, stay tuned!

## Previous releases:

### v1.30<br/>
Download: <b><a href="https://kustom.rocks/download/Konsole/23">Google Play APK</a></b><br/>
Build: 1.30<br/>
Size: 19.82mb<br/>
Uploaded: 2024-04-03T14:24:36.965Z<br/>
Release Notes:
None

### v1.20<br/>
Download: <b><a href="https://kustom.rocks/download/Konsole/21">Google Play APK</a></b><br/>
Build: 1.20<br/>
Size: 18.68mb<br/>
Uploaded: 2023-07-24T10:38:43.908Z<br/>
Release Notes:
None

### v1.18<br/>
Download: <b><a href="https://kustom.rocks/download/Konsole/19">Google Play APK</a></b><br/>
Build: 1.18<br/>
Size: 18.68mb<br/>
Uploaded: 2023-07-20T09:02:07.470Z<br/>
Release Notes:
None

### v1.14<br/>
Download: <b><a href="https://kustom.rocks/download/Konsole/17">Google Play APK</a></b><br/>
Build: 1.14<br/>
Size: 18.68mb<br/>
Uploaded: 2023-07-10T07:03:06.515Z<br/>
Release Notes:
None

### v1.10<br/>
Download: <b><a href="https://kustom.rocks/download/Konsole/15">Google Play APK</a></b><br/>
Build: 1.10<br/>
Size: 14.60mb<br/>
Uploaded: 2023-07-03T13:50:32.366Z<br/>
Release Notes:
None

### v1.06-staging<br/>
Download: <b><a href="https://kustom.rocks/download/Konsole/12">Google Play APK</a></b><br/>
Build: 1.06-staging<br/>
Size: 14.62mb<br/>
Uploaded: 2023-05-26T12:55:24.822Z<br/>
Release Notes:
None

### v1.05<br/>
Download: <b><a href="https://kustom.rocks/download/Konsole/10">Google Play APK</a></b><br/>
Build: 1.05<br/>
Size: 14.57mb<br/>
Uploaded: 2023-05-24T15:03:00.372Z<br/>
Release Notes:
None

### v1.03<br/>
Download: <b><a href="https://kustom.rocks/download/Konsole/8">Google Play APK</a></b><br/>
Build: 1.03<br/>
Size: 14.57mb<br/>
Uploaded: 2023-05-24T14:21:04.367Z<br/>
Release Notes:
None

### v1.02<br/>
Download: <b><a href="https://kustom.rocks/download/Konsole/6">Google Play APK</a></b><br/>
Build: 1.02<br/>
Size: 14.57mb<br/>
Uploaded: 2023-05-24T12:32:59.557Z<br/>
Release Notes:
None

### v1.01<br/>
Download: <b><a href="https://kustom.rocks/download/Konsole/4">Google Play APK</a></b><br/>
Build: 1.01<br/>
Size: 14.57mb<br/>
Uploaded: 2023-05-24T10:23:17.456Z<br/>
Release Notes:
None

### v1.0<br/>
Download: <b><a href="https://kustom.rocks/download/Konsole/2">Google Play APK</a></b><br/>
Build: 1.0<br/>
Size: 14.57mb<br/>
Uploaded: 2023-05-24T08:42:04.581Z<br/>
Release Notes:
None
