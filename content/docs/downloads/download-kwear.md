---
title: Kwear
type: docs
categories:
  - Downloads
tags:
  - kwear
  - apk
  - aosp
  - changelog
---
<!-- <img src="https://appcenter-filemanagement-distrib1ede6f06e.azureedge.net/991c6b2b-c6e7-48f5-987a-4adbad3ea14f/9w.png?sv=2019-07-07&sr=c&sig=MnapeZUe6aHDhs4SnXLFG0SIz1P%2FO%2FnHNon6PuXdB0A%3D&se=2024-06-27T13%3A30%3A59Z&sp=r" width="50" /> -->
# Kwear downloads
⌚ **This APK is for Wear OS only**

## Latest stable: 0.01
**Download**: <b><a href="https://kustom.rocks/download/Kwear/34">Google Play APK</a></b><br/>
Build: 0.01b417213<br/>
Size: 24.03mb<br/>
Uploaded: 2024-06-20T13:30:57.934Z<br/>






### Previous builds 
  - <a href="https://kustom.rocks/download/Kwear/33">0.01b417112</a>
  - <a href="https://kustom.rocks/download/Kwear/31">0.01b410013</a>
  - <a href="https://kustom.rocks/download/Kwear/29">0.01b408115</a>
  - <a href="https://kustom.rocks/download/Kwear/28">0.01b407209</a>
  - <a href="https://kustom.rocks/download/Kwear/27">0.01b406816</a>


## No beta right now, stay tuned!

## Previous releases:
