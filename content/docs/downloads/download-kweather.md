---
title: Kweather
type: docs
categories:
  - Downloads
tags:
  - kweather
  - apk
  - aosp
  - changelog
---
<!-- <img src="https://appcenter-filemanagement-distrib4ede6f06e.azureedge.net/4bc1169e-6fe2-4f5f-aff2-876406d75c48/9w.png?sv=2019-07-07&sr=c&sig=%2FIWbB3%2Bxh57LsX7NwAKCl7TKCPA6J1bgPRh7NNnfJRM%3D&se=2024-06-21T06%3A55%3A35Z&sp=r" width="50" /> -->
# Kweather downloads


## Latest stable: 1.30
**Download**: <b><a href="https://kustom.rocks/download/Kweather/31">Google Play APK</a></b><br/>
Build: 1.30b416606<br/>
Size: 12.88mb<br/>
Uploaded: 2024-06-14T06:55:33.408Z<br/>






## No beta right now, stay tuned!

## Previous releases:

### v1.21<br/>
Download: <b><a href="https://kustom.rocks/download/Kweather/29">Google Play APK</a></b><br/>
Build: 1.21b308209<br/>
Size: 11.12mb<br/>
Uploaded: 2023-03-23T09:19:36.834Z<br/>
Release Notes:
<ul style="margin: 0"><li>Fixes crash in querying details</li></ul>

### v1.20<br/>
Download: <b><a href="https://kustom.rocks/download/Kweather/28">Google Play APK</a></b><br/>
Build: 1.20b22811<br/>
Size: 5.17mb<br/>
Uploaded: 2020-08-15T11:46:23.882Z<br/>
Release Notes:
<ul style="margin: 0"><li>Fixes crash in querying details</li></ul>

### v1.12<br/>
Download: <b><a href="https://kustom.rocks/download/Kweather/27">Google Play APK</a></b><br/>
Build: 1.12b921910<br/>
Size: 3.61mb<br/>
Uploaded: 2019-08-07T10:06:24.000Z<br/>
Release Notes:
<ul style="margin: 0"><li>Fixes crash in querying details</li></ul>

### v1.11<br/>
Download: <b><a href="https://kustom.rocks/download/Kweather/26">Google Play APK</a></b><br/>
Build: 1.11b919904<br/>
Size: 3.10mb<br/>
Uploaded: 2019-07-18T04:57:45.000Z<br/>
Release Notes:
<ul style="margin: 0"><li>Fixes crash in querying details</li></ul>

### v1.10<br/>
Download: <b><a href="https://kustom.rocks/download/Kweather/22">Google Play APK</a></b><br/>
Build: 1.10b917814beta<br/>
Size: 3.10mb<br/>
Uploaded: 2019-06-27T14:27:23.000Z<br/>
Release Notes:
<ul style="margin: 0"><li>Fixes providers cant be selected in the main app</li></ul>

### v1.09<br/>
Download: <b><a href="https://kustom.rocks/download/Kweather/19">Google Play APK</a></b><br/>
Build: 1.09b914413<br/>
Size: 3.07mb<br/>
Uploaded: 2019-05-24T13:18:26.000Z<br/>
Release Notes:
<ul style="margin: 0"><li>Support for UV Index and Clouds</li><li>Fixed hourly weather in Accu</li><li>Fixed wind speed in Accu</li><li>Small other fixes</li><li>Language update</li></ul>

### v1.08<br/>
Download: <b><a href="https://kustom.rocks/download/Kweather/15">Google Play APK</a></b><br/>
Build: 1.08b901015<br/>
Size: 3.65mb<br/>
Uploaded: 2019-01-10T15:27:17.000Z<br/>
Release Notes:
None

### v1.07<br/>
Download: <b><a href="https://kustom.rocks/download/Kweather/14">Google Play APK</a></b><br/>
Build: 1.07b824913<br/>
Size: 3.16mb<br/>
Uploaded: 2018-09-06T13:44:44.000Z<br/>
Release Notes:
None

### v1.05<br/>
Download: <b><a href="https://kustom.rocks/download/Kweather/13">Google Play APK</a></b><br/>
Build: 1.05b801915<br/>
Size: 2.68mb<br/>
Uploaded: 2018-01-19T15:33:36.000Z<br/>
Release Notes:
None

### v1.04<br/>
Download: <b><a href="https://kustom.rocks/download/Kweather/11">Google Play APK</a></b><br/>
Build: 1.04b724311<br/>
Size: 2.61mb<br/>
Uploaded: 2017-08-31T11:54:12.000Z<br/>
Release Notes:
None

### v1.03<br/>
Download: <b><a href="https://kustom.rocks/download/Kweather/10">Google Play APK</a></b><br/>
Build: 1.03b721509<br/>
Size: 2.58mb<br/>
Uploaded: 2017-08-03T09:39:06.000Z<br/>
Release Notes:
None
