---
title: KLCK
type: docs
categories:
  - Downloads
tags:
  - klck
  - apk
  - aosp
  - changelog
---
<!-- <img src="https://appcenter-filemanagement-distrib5ede6f06e.azureedge.net/b87930a9-8031-4538-92f2-bdff27b8e721/9w.png?sv=2019-07-07&sr=c&sig=nl%2BXHSucdri2jgm1IgXtg8KEpHj22h1aPChgCL4ttVU%3D&se=2024-06-27T14%3A56%3A29Z&sp=r" width="50" /> -->
# KLCK downloads


## Latest stable: 3.75
**Download**: <b><a href="https://kustom.rocks/download/KLCK/503">Google Play APK</a></b><br/>
Build: 3.75b410013<br/>
Size: 30.12mb<br/>
Uploaded: 2024-04-09T14:32:54.919Z<br/>

### Variants
  - **Huawei**: <a href="https://kustom.rocks/download/KLCK/490">3.75b331812huawei</a> (App Gallery APK) 







## Latest beta: 3.76
**Download**: <b><a href="https://kustom.rocks/download/KLCK/504">Google Play APK</a></b><br/>
Build: 3.76b417112beta<br/>
Size: 29.91mb<br/>
Uploaded: 2024-06-19T13:32:18.723Z<br/>


### Variants
  - **Huawei**: <a href="https://kustom.rocks/download/KLCK/508">3.76b417214huawei</a> (App Gallery APK)
  - **AOSP**: <a href="https://kustom.rocks/download/KLCK/505">3.76b417213aosp</a> (No PRO required, ADS supported, no Google Services)
 



### Previous builds


## Previous releases:

### v3.74<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/489">Google Play APK</a></b><br/>
Build: 3.74b331712<br/>
Size: 27.86mb<br/>
Uploaded: 2023-11-13T12:45:18.671Z<br/>
Release Notes:
None

### v3.73<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/460">Google Play APK</a></b><br/>
Build: 3.73b314511<br/>
Size: 27.25mb<br/>
Uploaded: 2023-05-25T11:11:21.430Z<br/>
Release Notes:
<ul style="margin: 0"><li>Added new free path in Shapes, draw ANYTHING you want! Check out https://kustom.rocks/svgpath</li><li>Added confirm dialog on preset delete</li><li>You can now spell numbers and clock in Japanese</li><li>You can now parse dates in ISO or other formats via dp()</li><li>Hiding duplicates in font picker</li><li>Fixed issues with preset import on file open</li><li>Fixed sizing issues on foldable screens</li><li>Fixed flow issue not hiding when closing bottom menu</li><li>Fixed gradients in squircle shape</li><li>Fix KLWP disappearing objects when close to the edge</li><li>Fix issue on Samsung devices when adding a shortcut</li><li>Fix Kustom forgets Tasker vars not updated for more than 6 days</li></ul>

### v3.72<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/455">Google Play APK</a></b><br/>
Build: 3.72b310707<br/>
Size: 27.89mb<br/>
Uploaded: 2023-04-17T07:53:23.408Z<br/>
Release Notes:
<ul style="margin: 0"><li>You can now set globals using an URI like kwgt://global/name/value</li><li>Fixed new presets not shown after export or import</li><li>Fixes lag on KLWP in some preset</li></ul>

### v3.71<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/450">Google Play APK</a></b><br/>
Build: 3.71b308214<br/>
Size: 27.90mb<br/>
Uploaded: 2023-03-23T15:29:01.464Z<br/>
Release Notes:
<ul style="margin: 0"><li>You can now C style use /* comments */ inside $kustom expressions$</li><li>Brand new export dialog, feedback welcome!</li><li>You can now export as image</li><li>Flows can open URI and Intent links (so you can set Tasker vars)</li><li>Flows can store any type of globals not just text globals</li><li>Fixes new preset being re-exported causing duplicates</li><li>Fixes music player info providing wrong package name</li><li>Fixes feels like temperature not using provider data in AccuWeather</li><li>Fixed "$wi(tempu)$" not changing when settings changed</li></ul>

### v3.70<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/439">Google Play APK</a></b><br/>
Build: 3.70b303210<br/>
Size: 25.61mb<br/>
Uploaded: 2023-02-01T15:41:52.945Z<br/>
Release Notes:
<ul style="margin: 0"><li>Added Kustom Flows to bring you where no one has gone before https://kustom.rocks/flows</li><li>Added Secret Globals to store sensitive data like API keys in locked komponents</li><li>Added support for remote messages (http post from the internet), see https://kustom.rocks/remotemsg</li><li>Added support for Material You "Monet" accent and neutral colors via $si(sysca1/a2/a3/n1/n2, shadelevel)$ function https://kustom.rocks/dyncolors</li><li>KLWP now detects if its zoomed via $si(wgzoomed)$, can be used on some launcher to understand when drawer or recents are opened</li><li>Improved speed of listing entries / fonts from SD</li><li>Fixed KWGT not correctly updating when a global was changed</li><li>Fixed YRNO weather provider current conditions not always displayed</li></ul>

### v3.63<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/398">Google Play APK</a></b><br/>
Build: 3.63b228708<br/>
Size: 22.78mb<br/>
Uploaded: 2022-10-14T09:58:31.554Z<br/>
Release Notes:
<ul style="margin: 0"><li>New lv() function can set local variables</li><li>In KWGT si(darkmode) now only returns system dark mode</li><li>Added si(darkwp) that returns 1 when wallpaper colors prefer a dark theme</li><li>Formula faves are now also stored on external storage as a backup measure</li></ul>

### v3.62<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/386">Google Play APK</a></b><br/>
Build: 3.62b224415<br/>
Size: 22.36mb<br/>
Uploaded: 2022-09-01T17:07:07.021Z<br/>
Release Notes:
<ul style="margin: 0"><li>Font picker now shows current text as preview</li><li>Font picker UI improvements</li><li>Fixed duplicate issue on exporting some preset</li><li>Fixes notifications export issues</li><li>Fixes random image and file not working even in Kustom subfolders</li></ul>

### v3.61<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/377">Google Play APK</a></b><br/>
Build: 3.61b223012<br/>
Size: 22.43mb<br/>
Uploaded: 2022-08-18T14:54:39.949Z<br/>
Release Notes:
<ul style="margin: 0"><li>Improved save speed for presets with lots of resources</li><li>Slightly improved external font / font packs loading speed</li><li>Fixed KLWP 5 secs delay</li><li>Fixed font picker not remembering scroll position</li><li>Fixed alpha ordering in font picker</li><li>Fixed recent/faves not working for local storage presets</li></ul>

### v3.60<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/356">Google Play APK</a></b><br/>
Build: 3.60b220710<br/>
Size: 22.41mb<br/>
Uploaded: 2022-07-26T11:58:23.601Z<br/>
Release Notes:
<ul style="margin: 0"><li>Fixed local fonts not saved</li></ul>

### v3.59<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/353">Google Play APK</a></b><br/>
Build: 3.59b220615<br/>
Size: 22.41mb<br/>
Uploaded: 2022-07-25T16:56:04.997Z<br/>
Release Notes:
<ul style="margin: 0"><li>Hot fix exporting presets will not store resources</li></ul>

### v3.58<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/341">Google Play APK</a></b><br/>
Build: 3.58b217313<br/>
Size: 22.41mb<br/>
Uploaded: 2022-06-22T15:02:19.703Z<br/>
Release Notes:
<ul style="margin: 0"><li>Removed access to SD Card due to new security policies, storage migration will be requested</li><li>New font picker now shows Google fonts automatically</li><li>KLWP workaround for Samsung Android 12 bug</li><li>KLWP ignores wallpaper color for dark mode</li><li>You can purchase KWGT without separate pro key</li><li>A big thanks to all the users keeping translations up to date! You rock!</li></ul>

### v3.57<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/295">Google Play APK</a></b><br/>
Build: 3.57b121813<br/>
Size: 19.13mb<br/>
Uploaded: 2021-08-06T14:58:19.830Z<br/>
Release Notes:
<ul style="margin: 0"><li>Packs are now correctly showing newer items first in loader</li><li>Added Projekt launcher support on KLWP</li><li>Removed Yahoo weather :(</li></ul>

### v3.56<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/288">Google Play APK</a></b><br/>
Build: 3.56b114416<br/>
Size: 19.06mb<br/>
Uploaded: 2021-05-24T17:02:59.554Z<br/>
Release Notes:
<ul style="margin: 0"><li>Fixes color picker not showing recent colors</li><li>Fixes load preset tasker action</li><li>Fixes load preset from file explorer / telegram</li><li>Fixes color picker unusable on small screens</li></ul>

### v3.55<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/284">Google Play APK</a></b><br/>
Build: 3.55b112308<br/>
Size: 19.08mb<br/>
Uploaded: 2021-05-03T09:45:56.645Z<br/>
Release Notes:
<ul style="margin: 0"><li>New color picker, feedback welcome!</li><li>Added support for global folders</li><li>Fix issues with TF and settings on some device</li><li>Fix empty filter locks you in the loader</li></ul>

### v3.54<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/277">Google Play APK</a></b><br/>
Build: 3.54b106810<br/>
Size: 18.77mb<br/>
Uploaded: 2021-03-09T11:40:24.645Z<br/>
Release Notes:
<ul style="margin: 0"><li>Fix loading directly from packs dashboard not working</li><li>Fix issues with location search</li><li>Language updates</li></ul>

### v3.53<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/272">Google Play APK</a></b><br/>
Build: 3.53b104015<br/>
Size: 18.57mb<br/>
Uploaded: 2021-02-09T16:20:34.101Z<br/>
Release Notes:
<ul style="margin: 0"><li>New purchase dialog, feedback welcome</li><li>You can now browse installed packs in the main loader</li><li>Fix YrNo will stop working 31st of March</li><li>Fix KLWP sometimes launching wrong app</li><li>Fix pressing edit in the widget will then make load preset fail</li><li>Fix issues on Samsung when in Airplane Mode</li><li>Fix preview scaling in KLWP loader</li><li>Fix hardcoded max at 720 in number globals</li></ul>

### v3.52<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/260">Google Play APK</a></b><br/>
Build: 3.52b102617<br/>
Size: 18.49mb<br/>
Uploaded: 2021-01-26T20:06:44.965Z<br/>
Release Notes:
<ul style="margin: 0"><li>New loader window, feedback welcome!</li><li>Added tc(url) to encode URLs</li><li>Added si(powersave) to show when power save is on</li><li>Fixed traditional and simplified Chinese in language settings</li><li>Fixed delay to launch in Android 11 for KLWP</li><li>Fixed location search not working in some language</li><li>Fixed crop not working in the editor</li><li>Fixed BT state not updated on change</li><li>Fixed SIM count on some device</li></ul>

### v3.51<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/240">Google Play APK</a></b><br/>
Build: 3.51b30911<br/>
Size: 18.13mb<br/>
Uploaded: 2020-11-04T11:43:09.769Z<br/>
Release Notes:
<ul style="margin: 0"><li>New intro and settings</li><li>New KLWP control refresh rate in the advanced settings</li><li>Fixes Chinese language cannot be forced</li><li>Fixed some music players not being recognized</li><li>Fixes slow downs / battery issues when using palette formulas</li><li>Fixes battery duration / last plugged issues on Android 11</li></ul>

### v3.50<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/218">Google Play APK</a></b><br/>
Build: 3.50b28512<br/>
Size: 12.98mb<br/>
Uploaded: 2020-10-11T12:58:39.326Z<br/>
Release Notes:
<ul style="margin: 0"><li>HOTFIX fonts and icon fonts picker not working</li></ul>

### v3.49<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/216">Google Play APK</a></b><br/>
Build: 3.49b27509<br/>
Size: 12.97mb<br/>
Uploaded: 2020-10-01T09:35:55.079Z<br/>
Release Notes:
<ul style="margin: 0"><li>Added support for battery level of BT devices (Android 8 or newer)</li><li>Added support for 5G network type</li><li>Added squircle shape so you can get that IOS picture widget :)</li><li>Slightly faster export / import</li><li>Removed toggle WiFi and BT due to API 29 upgrade, sorry, blame Google</li><li>KLWP has a new option to handle raw touch on launchers where touch does not work</li><li>You can copy and paste globals</li><li>Fixed TS unit wrong</li><li>Fixed TU not working with negative values</li><li>KLCK Fixed support for Android 10</li><li>KLCK Fixed lock not displaying on Android 8 or newer without system lock</li><li>KLCK Fixed music visualizer</li></ul>

### v3.48<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/202">Google Play APK</a></b><br/>
Build: 3.48b21016<br/>
Size: 12.67mb<br/>
Uploaded: 2020-07-28T17:06:08.902Z<br/>
Release Notes:
<ul style="margin: 0"><li>KLWP Added support for music visualization</li><li>KLWP General availability of Movie Module</li><li>Added rounded corners to triangles, exagons</li><li>Added CJK for Chinese chars in tc(type)</li><li>Weather plugin now supports also WeatherBit and, for Australia, WillyWeather</li><li>Fixed force weather update touch action not working</li><li>Fixed download/upload speed showing wrong values realtime</li><li>Fixed animation formulas not showing formula value</li></ul>

### v3.47<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/192">Google Play APK</a></b><br/>
Build: 3.47b16716<br/>
Size: 12.90mb<br/>
Uploaded: 2020-06-15T16:31:35.254Z<br/>
Release Notes:
<ul style="margin: 0"><li>New you can select multiple preferred music players in the settings</li><li>Increased max loops in for loops</li><li>Fix preferred music player not forced when other players detected</li><li>Fix YT Music and Google Podcasts not being detected as players</li></ul>

### v3.46<br/>
Download: <b><a href="https://kustom.rocks/download/KLCK/188">Google Play APK</a></b><br/>
Build: 3.46b14609<br/>
Size: 12.77mb<br/>
Uploaded: 2020-05-25T09:30:43.628Z<br/>
Release Notes:
<ul style="margin: 0"><li>New scale modes in Bitmap Module (fit height, center fit and center crop)</li><li>New text module fit box mode to control height and width</li><li>New hex and decimal conversion in mu()</li><li>New count text occurences with tc(count)</li><li>Fix palette never returning black as a color</li><li>Fix in current not working on some device</li><li>Fix ce(contrast) returning black too often</li><li>Fix forcing Chinese language not working</li><li>KLWP Fix visibility issues during animations</li></ul>
