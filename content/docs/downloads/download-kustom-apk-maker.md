---
title: Kustom-APK-Maker
type: docs
categories:
  - Downloads
tags:
  - kustom-apk-maker
  - apk
  - aosp
  - changelog
---
<!-- <img src="https://appcenter-filemanagement-distrib1ede6f06e.azureedge.net/031bcd99-6eb8-4dd5-9629-09c6103b01a2/ic_launcher.png?sv=2019-07-07&sr=c&sig=XELOCf3RpBiOrGQ1JLGPps9Bc960%2FDtsbgUMVORnksw%3D&se=2024-06-22T02%3A22%3A02Z&sp=r" width="50" /> -->
# Kustom-APK-Maker downloads


## Latest stable: 0.21
**Download**: <b><a href="https://kustom.rocks/download/Kustom-APK-Maker/35">Google Play APK</a></b><br/>
Build: 0.21b920615<br/>
Size: 14.18mb<br/>
Uploaded: 2019-07-25T15:28:35.000Z<br/>







## Latest beta: 0.22
**Download**: <b><a href="https://kustom.rocks/download/Kustom-APK-Maker/37">Google Play APK</a></b><br/>
Build: 0.22b020908<br/>
Size: 14.29mb<br/>
Uploaded: 2020-07-27T09:02:53.099Z<br/>




### Previous builds
  - <a href="https://kustom.rocks/download/Kustom-APK-Maker/36">0.22b020615</a> 2020-07-24T15:54:31.665Z


## Previous releases:

### v0.20<br/>
Download: <b><a href="https://kustom.rocks/download/Kustom-APK-Maker/34">Google Play APK</a></b><br/>
Build: 0.20b902811<br/>
Size: 14.04mb<br/>
Uploaded: 2019-01-28T11:07:56.000Z<br/>
Release Notes:
None

### v0.19<br/>
Download: <b><a href="https://kustom.rocks/download/Kustom-APK-Maker/33">Google Play APK</a></b><br/>
Build: 0.19b902719<br/>
Size: 14.33mb<br/>
Uploaded: 2019-01-27T19:06:16.000Z<br/>
Release Notes:
None

### v0.18<br/>
Download: <b><a href="https://kustom.rocks/download/Kustom-APK-Maker/28">Google Play APK</a></b><br/>
Build: 0.18b836215<br/>
Size: 13.34mb<br/>
Uploaded: 2018-12-28T15:52:36.000Z<br/>
Release Notes:
None

### v0.17<br/>
Download: <b><a href="https://kustom.rocks/download/Kustom-APK-Maker/26">Google Play APK</a></b><br/>
Build: 0.17b801011<br/>
Size: 13.13mb<br/>
Uploaded: 2018-01-10T11:06:17.000Z<br/>
Release Notes:
None

### v0.16<br/>
Download: <b><a href="https://kustom.rocks/download/Kustom-APK-Maker/24">Google Play APK</a></b><br/>
Build: 0.16b736317<br/>
Size: 13.12mb<br/>
Uploaded: 2017-12-29T17:08:04.000Z<br/>
Release Notes:
None

### v0.14<br/>
Download: <b><a href="https://kustom.rocks/download/Kustom-APK-Maker/21">Google Play APK</a></b><br/>
Build: 0.14b735309<br/>
Size: 12.93mb<br/>
Uploaded: 2017-12-19T09:35:23.000Z<br/>
Release Notes:
None

### v0.12<br/>
Download: <b><a href="https://kustom.rocks/download/Kustom-APK-Maker/17">Google Play APK</a></b><br/>
Build: 0.12b734717<br/>
Size: 12.58mb<br/>
Uploaded: 2017-12-13T17:07:15.000Z<br/>
Release Notes:
None

### v0.10<br/>
Download: <b><a href="https://kustom.rocks/download/Kustom-APK-Maker/14">Google Play APK</a></b><br/>
Build: 0.10b727609<br/>
Size: 13.27mb<br/>
Uploaded: 2017-10-03T09:33:35.000Z<br/>
Release Notes:
None

### v0.09<br/>
Download: <b><a href="https://kustom.rocks/download/Kustom-APK-Maker/13">Google Play APK</a></b><br/>
Build: 0.09b725315<br/>
Size: 13.26mb<br/>
Uploaded: 2017-09-10T15:26:48.000Z<br/>
Release Notes:
None

### v0.08<br/>
Download: <b><a href="https://kustom.rocks/download/Kustom-APK-Maker/10">Google Play APK</a></b><br/>
Build: 0.08b709014<br/>
Size: 12.82mb<br/>
Uploaded: 2017-03-31T15:01:00.000Z<br/>
Release Notes:
None
