---
title: KWCH
type: docs
categories:
  - Downloads
tags:
  - kwch
  - apk
  - aosp
  - changelog
---
<!-- <img src="https://appcenter-filemanagement-distrib1ede6f06e.azureedge.net/4bdc195f-5c05-43d5-8f97-140f6be67c8d/9w.png?sv=2019-07-07&sr=c&sig=JW1H%2FojyX9LDJpsTCr59F7yvYgvD7ZfMlT2okcRr4UE%3D&se=2024-06-27T13%3A26%3A51Z&sp=r" width="50" /> -->
# KWCH downloads


## Latest stable: 3.75
**Download**: <b><a href="https://kustom.rocks/download/KWCH/64">Google Play APK</a></b><br/>
Build: 3.75b410013<br/>
Size: 30.89mb<br/>
Uploaded: 2024-04-09T14:30:47.982Z<br/>







## Latest beta: 3.76
**Download**: <b><a href="https://kustom.rocks/download/KWCH/66">Google Play APK</a></b><br/>
Build: 3.76b417213beta<br/>
Size: 30.57mb<br/>
Uploaded: 2024-06-20T13:26:49.038Z<br/>




### Previous builds
  - <a href="https://kustom.rocks/download/KWCH/65">3.76b417112beta</a> 2024-06-19T13:32:16.697Z


## Previous releases:

### v3.74<br/>
Download: <b><a href="https://kustom.rocks/download/KWCH/42">Google Play APK</a></b><br/>
Build: 3.74b331712<br/>
Size: 28.45mb<br/>
Uploaded: 2023-11-13T12:45:49.529Z<br/>
Release Notes:
None
