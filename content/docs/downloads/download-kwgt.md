---
title: KWGT
type: docs
categories:
  - Downloads
tags:
  - kwgt
  - apk
  - aosp
  - changelog
---
<!-- <img src="https://appcenter-filemanagement-distrib3ede6f06e.azureedge.net/98dc7b77-5d76-451d-8388-29af542c3d30/9w.png?sv=2019-07-07&sr=c&sig=CywbJZab4DXgUM%2FOZ5PNc6M15j0aZ9YvaDlvgqsrFcU%3D&se=2024-06-27T14%3A56%3A57Z&sp=r" width="50" /> -->
# KWGT downloads


## Latest stable: 3.75
**Download**: <b><a href="https://kustom.rocks/download/KWGT/772">Google Play APK</a></b><br/>
Build: 3.75b410013<br/>
Size: 32.71mb<br/>
Uploaded: 2024-04-09T14:30:53.248Z<br/>







## Latest beta: 3.76
**Download**: <b><a href="https://kustom.rocks/download/KWGT/774">Google Play APK</a></b><br/>
Build: 3.76b417112beta<br/>
Size: 32.50mb<br/>
Uploaded: 2024-06-19T13:32:15.426Z<br/>


### Variants
  - **Huawei**: <a href="https://kustom.rocks/download/KWGT/777">3.76b417214huawei</a> (App Gallery APK)
  - **AOSP**: <a href="https://kustom.rocks/download/KWGT/775">3.76b417214aosp</a> (No PRO required, ADS supported, no Google Services)
 



### Previous builds


## Previous releases:

### v3.74<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/759">Google Play APK</a></b><br/>
Build: 3.74b331712<br/>
Size: 30.66mb<br/>
Uploaded: 2023-11-13T12:45:16.863Z<br/>
Release Notes:
None

### v3.73<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/736">Google Play APK</a></b><br/>
Build: 3.73b314510<br/>
Size: 30.26mb<br/>
Uploaded: 2023-05-25T11:00:07.461Z<br/>
Release Notes:
<ul style="margin: 0"><li>Added new free path in Shapes, draw ANYTHING you want! Check out https://kustom.rocks/svgpath</li><li>Added confirm dialog on preset delete</li><li>You can now spell numbers and clock in Japanese</li><li>You can now parse dates in ISO or other formats via dp()</li><li>Hiding duplicates in font picker</li><li>Fixed issues with preset import on file open</li><li>Fixed sizing issues on foldable screens</li><li>Fixed flow issue not hiding when closing bottom menu</li><li>Fixed gradients in squircle shape</li><li>Fix KLWP disappearing objects when close to the edge</li><li>Fix issue on Samsung devices when adding a shortcut</li><li>Fix Kustom forgets Tasker vars not updated for more than 6 days</li></ul>

### v3.72<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/730">Google Play APK</a></b><br/>
Build: 3.72b310707<br/>
Size: 30.76mb<br/>
Uploaded: 2023-04-17T07:34:29.201Z<br/>
Release Notes:
<ul style="margin: 0"><li>You can now set globals using an URI like kwgt://global/name/value</li><li>Fixed new presets not shown after export or import</li><li>Fixes lag on KLWP in some preset</li></ul>

### v3.71<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/725">Google Play APK</a></b><br/>
Build: 3.71b308214<br/>
Size: 30.75mb<br/>
Uploaded: 2023-03-23T15:04:01.332Z<br/>
Release Notes:
<ul style="margin: 0"><li>You can now C style use /* comments */ inside $kustom expressions$</li><li>Brand new export dialog, feedback welcome!</li><li>You can now export as image</li><li>Flows can open URI and Intent links (so you can set Tasker vars)</li><li>Flows can store any type of globals not just text globals</li><li>Fixes new preset being re-exported causing duplicates</li><li>Fixes music player info providing wrong package name</li><li>Fixes feels like temperature not using provider data in AccuWeather</li><li>Fixed "$wi(tempu)$" not changing when settings changed</li></ul>

### v3.70<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/711">Google Play APK</a></b><br/>
Build: 3.70b303210<br/>
Size: 28.45mb<br/>
Uploaded: 2023-02-01T15:23:11.260Z<br/>
Release Notes:
<ul style="margin: 0"><li>Added Kustom Flows to bring you where no one has gone before https://kustom.rocks/flows</li><li>Added Secret Globals to store sensitive data like API keys in locked komponents</li><li>Added support for remote messages (http post from the internet), see https://kustom.rocks/remotemsg</li><li>Added support for Material You "Monet" accent and neutral colors via $si(sysca1/a2/a3/n1/n2, shadelevel)$ function https://kustom.rocks/dyncolors</li><li>KLWP now detects if its zoomed via $si(wgzoomed)$, can be used on some launcher to understand when drawer or recents are opened</li><li>Improved speed of listing entries / fonts from SD</li><li>Fixed KWGT not correctly updating when a global was changed</li><li>Fixed YRNO weather provider current conditions not always displayed</li></ul>

### v3.63<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/659">Google Play APK</a></b><br/>
Build: 3.63b228708<br/>
Size: 25.63mb<br/>
Uploaded: 2022-10-14T09:37:42.953Z<br/>
Release Notes:
<ul style="margin: 0"><li>New lv() function can set local variables</li><li>In KWGT si(darkmode) now only returns system dark mode</li><li>Added si(darkwp) that returns 1 when wallpaper colors prefer a dark theme</li><li>Formula faves are now also stored on external storage as a backup measure</li></ul>

### v3.62<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/647">Google Play APK</a></b><br/>
Build: 3.62b224415<br/>
Size: 25.20mb<br/>
Uploaded: 2022-09-01T16:54:21.971Z<br/>
Release Notes:
<ul style="margin: 0"><li>Font picker now shows current text as preview</li><li>Font picker UI improvements</li><li>Fixed duplicate issue on exporting some preset</li><li>Fixes notifications export issues</li><li>Fixes random image and file not working even in Kustom subfolders</li></ul>

### v3.61<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/638">Google Play APK</a></b><br/>
Build: 3.61b223012<br/>
Size: 25.27mb<br/>
Uploaded: 2022-08-18T14:36:57.968Z<br/>
Release Notes:
<ul style="margin: 0"><li>Improved save speed for presets with lots of resources</li><li>Slightly improved external font / font packs loading speed</li><li>Fixed KLWP 5 secs delay</li><li>Fixed font picker not remembering scroll position</li><li>Fixed alpha ordering in font picker</li><li>Fixed recent/faves not working for local storage presets</li></ul>

### v3.60<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/617">Google Play APK</a></b><br/>
Build: 3.60b220710<br/>
Size: 25.25mb<br/>
Uploaded: 2022-07-26T11:44:54.486Z<br/>
Release Notes:
<ul style="margin: 0"><li>Fixed local fonts not saved</li></ul>

### v3.59<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/614">Google Play APK</a></b><br/>
Build: 3.59b220615<br/>
Size: 25.25mb<br/>
Uploaded: 2022-07-25T16:38:30.129Z<br/>
Release Notes:
<ul style="margin: 0"><li>Hot fix exporting presets will not store resources</li></ul>

### v3.58<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/602">Google Play APK</a></b><br/>
Build: 3.58b217313<br/>
Size: 25.25mb<br/>
Uploaded: 2022-06-22T14:49:48.756Z<br/>
Release Notes:
<ul style="margin: 0"><li>Removed access to SD Card due to new security policies, storage migration will be requested</li><li>New font picker now shows Google fonts automatically</li><li>KLWP workaround for Samsung Android 12 bug</li><li>KLWP ignores wallpaper color for dark mode</li><li>You can purchase KWGT without separate pro key</li><li>A big thanks to all the users keeping translations up to date! You rock!</li></ul>

### v3.57<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/552">Google Play APK</a></b><br/>
Build: 3.57b121813<br/>
Size: 21.97mb<br/>
Uploaded: 2021-08-06T14:46:37.484Z<br/>
Release Notes:
<ul style="margin: 0"><li>Packs are now correctly showing newer items first in loader</li><li>Added Projekt launcher support on KLWP</li><li>Removed Yahoo weather :(</li></ul>

### v3.56<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/546">Google Play APK</a></b><br/>
Build: 3.56b114416<br/>
Size: 21.90mb<br/>
Uploaded: 2021-05-24T16:48:52.387Z<br/>
Release Notes:
<ul style="margin: 0"><li>Fixes color picker not showing recent colors</li><li>Fixes load preset tasker action</li><li>Fixes load preset from file explorer / telegram</li><li>Fixes color picker unusable on small screens</li></ul>

### v3.55<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/538">Google Play APK</a></b><br/>
Build: 3.55b112308<br/>
Size: 21.93mb<br/>
Uploaded: 2021-05-03T09:35:08.769Z<br/>
Release Notes:
<ul style="margin: 0"><li>New color picker, feedback welcome!</li><li>Added support for global folders</li><li>Fix issues with TF and settings on some device</li><li>Fix empty filter locks you in the loader</li></ul>

### v3.54<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/530">Google Play APK</a></b><br/>
Build: 3.54b106810<br/>
Size: 21.62mb<br/>
Uploaded: 2021-03-09T11:08:59.407Z<br/>
Release Notes:
<ul style="margin: 0"><li>Fix loading directly from packs dashboard not working</li><li>Fix issues with location search</li><li>Language updates</li></ul>

### v3.53<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/525">Google Play APK</a></b><br/>
Build: 3.53b104015<br/>
Size: 21.40mb<br/>
Uploaded: 2021-02-09T16:11:36.889Z<br/>
Release Notes:
<ul style="margin: 0"><li>New purchase dialog, feedback welcome</li><li>You can now browse installed packs in the main loader</li><li>Fix YrNo will stop working 31st of March</li><li>Fix KLWP sometimes launching wrong app</li><li>Fix pressing edit in the widget will then make load preset fail</li><li>Fix issues on Samsung when in Airplane Mode</li><li>Fix preview scaling in KLWP loader</li><li>Fix hardcoded max at 720 in number globals</li></ul>

### v3.52<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/506">Google Play APK</a></b><br/>
Build: 3.52b102617<br/>
Size: 21.35mb<br/>
Uploaded: 2021-01-26T19:43:41.350Z<br/>
Release Notes:
<ul style="margin: 0"><li>New loader window, feedback welcome!</li><li>Added tc(url) to encode URLs</li><li>Added si(powersave) to show when power save is on</li><li>Fixed traditional and simplified Chinese in language settings</li><li>Fixed delay to launch in Android 11 for KLWP</li><li>Fixed location search not working in some language</li><li>Fixed crop not working in the editor</li><li>Fixed BT state not updated on change</li><li>Fixed SIM count on some device</li></ul>

### v3.51<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/478">Google Play APK</a></b><br/>
Build: 3.51b31415<br/>
Size: 23.51mb<br/>
Uploaded: 2020-11-09T15:46:40.431Z<br/>
Release Notes:
<ul style="margin: 0"><li>New intro and settings</li><li>New KLWP control refresh rate in the advanced settings</li><li>Fixes Chinese language cannot be forced</li><li>Fixed some music players not being recognized</li><li>Fixes slow downs / battery issues when using palette formulas</li><li>Fixes battery duration / last plugged issues on Android 11</li></ul>

### v3.50<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/454">Google Play APK</a></b><br/>
Build: 3.50b28512<br/>
Size: 15.77mb<br/>
Uploaded: 2020-10-11T13:01:57.017Z<br/>
Release Notes:
<ul style="margin: 0"><li>HOTFIX fonts and icon fonts picker not working</li></ul>

### v3.49<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/451">Google Play APK</a></b><br/>
Build: 3.49b27509<br/>
Size: 15.77mb<br/>
Uploaded: 2020-10-01T09:36:15.854Z<br/>
Release Notes:
<ul style="margin: 0"><li>Added support for battery level of BT devices (Android 8 or newer)</li><li>Added support for 5G network type</li><li>Added squircle shape so you can get that IOS picture widget :)</li><li>Slightly faster export / import</li><li>Removed toggle WiFi and BT due to API 29 upgrade, sorry, blame Google</li><li>KLWP has a new option to handle raw touch on launchers where touch does not work</li><li>You can copy and paste globals</li><li>Fixed TS unit wrong</li><li>Fixed TU not working with negative values</li><li>KLCK Fixed support for Android 10</li><li>KLCK Fixed lock not displaying on Android 8 or newer without system lock</li><li>KLCK Fixed music visualizer</li></ul>

### v3.48<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/434">Google Play APK</a></b><br/>
Build: 3.48b21017<br/>
Size: 15.05mb<br/>
Uploaded: 2020-07-28T17:19:45.577Z<br/>
Release Notes:
<ul style="margin: 0"><li>KLWP Added support for music visualization</li><li>KLWP General availability of Movie Module</li><li>Added rounded corners to triangles, exagons</li><li>Added CJK for Chinese chars in tc(type)</li><li>Weather plugin now supports also WeatherBit and, for Australia, WillyWeather</li><li>Fixed force weather update touch action not working</li><li>Fixed download/upload speed showing wrong values realtime</li><li>Fixed animation formulas not showing formula value</li></ul>

### v3.47<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/422">Google Play APK</a></b><br/>
Build: 3.47b16716<br/>
Size: 15.27mb<br/>
Uploaded: 2020-06-15T16:31:38.446Z<br/>
Release Notes:
<ul style="margin: 0"><li>New you can select multiple preferred music players in the settings</li><li>Increased max loops in for loops</li><li>Fix preferred music player not forced when other players detected</li><li>Fix YT Music and Google Podcasts not being detected as players</li></ul>

### v3.46<br/>
Download: <b><a href="https://kustom.rocks/download/KWGT/418">Google Play APK</a></b><br/>
Build: 3.46b14609<br/>
Size: 15.14mb<br/>
Uploaded: 2020-05-25T09:28:14.083Z<br/>
Release Notes:
<ul style="margin: 0"><li>New scale modes in Bitmap Module (fit height, center fit and center crop)</li><li>New text module fit box mode to control height and width</li><li>New hex and decimal conversion in mu()</li><li>New count text occurences with tc(count)</li><li>Fix palette never returning black as a color</li><li>Fix in current not working on some device</li><li>Fix ce(contrast) returning black too often</li><li>Fix forcing Chinese language not working</li><li>KLWP Fix visibility issues during animations</li></ul>
