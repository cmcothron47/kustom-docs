baseURL: https://docs.kustom.rocks/
title: Kustom HQ

# Hugo config
enableRobotsTXT: true

# Book configuration
disablePathToLower: true
enableGitInfo: true

# Needed for mermaid/katex shortcodes
markup:
  goldmark:
    renderer:
      unsafe: true
  tableOfContents:
    startLevel: 1
  highlight:
    anchorLineNos: false
    codeFences: true
    guessSyntax: false
    hl_Lines: ""
    hl_inline: false
    lineAnchors: ""
    lineNoStart: 1
    lineNos: false
    lineNumbersInTable: true
    noClasses: false
    noHl: false
    style: monokai
    tabWidth: 4

# Main menu
menu:
  before:
    - identifier: channels
      name: "Official channels"
      pre: "<br/><b>"
      post: "</b>"
      weight: 10
    - name: "Reddit"
      url: "https://reddit.com/r/Kustom"
      weight: 20
      parent: channels
    - name: "Discord"
      url: "https://kustom.rocks/discord"
      weight: 40
      parent: channels
    - name: "Play Store"
      url: "https://kustom.rocks/store"
      weight: 100
      parent: channels
      post: "<br/>"
    - identifier: forums
      name: "Forums"
      pre: "<br/><b>"
      post: "</b>"
      weight: 20
    - name: "Share an idea"
      url: "https://kustom.rocks/ideas"
      weight: 10
      parent: forums
    - name: "Report a problem"
      url: "https://kustom.rocks/problems"
      weight: 20
      parent: forums
    - name: "Ask a question"
      url: "https://forum.kustom.rocks/c/general"
      weight: 20
      parent: forums
    - name: "Share your setup"
      url: "https://forum.kustom.rocks/c/share"
      weight: 20
      parent: forums
      post: "<br/><br/>"
  after: []

# Configuration
params:
  # (Optional, default light) Sets color theme: light, dark or auto.
  # Theme 'auto' switches between dark and light modes based on browser/os preferences
  BookTheme: "light"
  # (Optional, default true) Controls table of contents visibility on right side of pages.
  # Start and end levels can be controlled with markup.tableOfContents setting.
  # You can also specify this parameter per page in front matter.
  BookToC: false
  # (Optional, default none) Set the path to a logo for the book. If the logo is
  # /static/logo.png then the path would be logo.png
  BookLogo: /logo.png
  # (Optional, default docs) Specify root page to render child pages as menu.
  # Page is resoled by .GetPage function: https://gohugo.io/functions/getpage/
  # For backward compatibility you can set '*' to render all sections to menu. Acts same as '/'
  BookSection: docs
  # Set source repository location.
  # Used for 'Last Modified' and 'Edit this page' links.
  BookRepo: https://gitlab.com/kustom-industries/kustom-docs
  # (Optional, default 'commit') Specifies commit portion of the link to the page's last modified
  # commit hash for 'doc' page type.
  # Requires 'BookRepo' param.
  # Value used to construct a URL consisting of BookRepo/BookCommitPath/<commit-hash>
  # Github uses 'commit', Bitbucket uses 'commits'
  BookCommitPath: commit
  # Enable "Edit this page" links for 'doc' page type.
  # Disabled by default. Uncomment to enable. Requires 'BookRepo' param.
  # Edit path must point to root directory of repo.
  #BookEditPath: -/blob/main
  # Configure the date format used on the pages
  # - In git information
  # - In blog posts
  BookDateFormat: "02 January 2006"
  # (Optional, default true) Enables search function with flexsearch,
  # Index is built on fly, therefore it might slowdown your website.
  # Configuration for indexing can be adjusted in i18n folder per language.
  BookSearch: true
  # (Optional, default true) Enables comments template on pages
  # By default partals/docs/comments.html includes Disqus template
  # See https://gohugo.io/content-management/comments/#configure-disqus
  # Can be overwritten by same param in page frontmatter
  BookComments: false
  # /!\ This is an experimental feature, might be removed or changed at any time
  # (Optional, experimental, default false) Enables portable links and link checks in markdown pages.
  # Portable links meant to work with text editors and let you write markdown without {{< relref >}} shortcode
  # Theme will print warning if page referenced in markdown does not exists.
  BookPortableLinks: true
  # /!\ This is an experimental feature, might be removed or changed at any time
  # (Optional, experimental, default false) Enables service worker that caches visited pages and resources for offline use.
  BookServiceWorker: true
  # /!\ This is an experimental feature, might be removed or changed at any time
  # (Optional, experimental, default false) Enables a drop-down menu for translations only if a translation is present.
  BookTranslatedOnly: false

# Module imports
module:
  imports:
    path: 'github.com/alex-shpak/hugo-book'
